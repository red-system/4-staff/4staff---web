import React from "react";
import {PostData} from "../../services/api";
import {Redirect} from "react-router"
import GeneralData from "../../constants/generalData";

export default class AppComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            notification_id: props.match.params.notification_id,
            notification_type: props.match.params.notification_type,
        };
    }

    componentWillMount() {
        let data = {
            notification_id: this.state.notification_id,
        };
        PostData('/notification-read', data).then((result) => {

        })
    }

    render() {
        let URL = this.Route();
        return <Redirect to={URL}/>
    }

    Route = () => {
        let appUriUniq = GeneralData.appUriUniq;
        let url = '';

        switch (this.state.notification_type) {
            case "profil_perusahaan":
                url = GeneralData.primaryMenu.my_company.sub_menu["1"].route;
                break;
            case "struktur_organisasi":
                url = GeneralData.primaryMenu.my_company.sub_menu["8"].route;
                break;
            case "deskripsi_pekerjaan":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["5"].route;
                break;
            case "alur_pekerjaan":
                url = GeneralData.primaryMenu.my_company.sub_menu["2"].route;
                break;
            case "penghargaan_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["14"].route;
                break;
            case "penghargaan_panduan":
                url = GeneralData.primaryMenu.my_company.sub_menu["4"].route;
                break;
            case "sanksi_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["15"].route;
                break;
            case "sanksi_panduan":
                url = GeneralData.primaryMenu.my_company.sub_menu["5"].route;
                break;
            case "aturan_regulasi":
                url = GeneralData.primaryMenu.my_company.sub_menu["3"].route;
                break;
            case "posisi_staf":
                url = GeneralData.primaryMenu.my_company.sub_menu["8"].route;
                break;
            case "kontrak_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["6"].route;
                break;
            case "deskripsi_pekerjaan_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["5"].route;
                break;
            case "benefit_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["17"].route;
                break;
            case "gaji":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["3"].route;
                break;
            case "reimburse_hrd":
                url = GeneralData.primaryMenu.staff_management.sub_menu["8"].route;
                break;
            case "reimburse_staf":
                url = GeneralData.primaryMenu.key_activity.sub_menu["4"].route;
                break;
            case "learning_development_hrd":
                url = GeneralData.primaryMenu.staff_management.sub_menu["9"].route;
                break;
            case "learning_development_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["16"].route;
                break;
            case "resign_hrd":
                url = GeneralData.primaryMenu.staff_management.sub_menu["11"].route;
                break;
            case "resign_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["12"].route;
                break;
            case "pengumuman":
                url = GeneralData.primaryMenu.my_company.sub_menu["9"].route;
                break;
            case "berita":
                url = GeneralData.primaryMenu.my_company.sub_menu["10"].route;
                break;
            case "dayoff":
                url = GeneralData.primaryMenu.my_company.sub_menu["7"].route;
                break;
            case "cuti_hrd":
                url = GeneralData.primaryMenu.staff_attendence.sub_menu["4"].route;
                break;
            case "cuti_staf":
                url = GeneralData.primaryMenu.key_activity.sub_menu["3"].route;
                break;
            case "lembur_hrd":
                url = GeneralData.primaryMenu.staff_attendence.sub_menu["5"].route;
                break;
            case "lembur_staf":
                url = GeneralData.primaryMenu.key_activity.sub_menu["5"].route;
                break;
            case "kpi_hrd":
                url = GeneralData.primaryMenu.staff_management.sub_menu["10"].route;
                break;
            case "kpi_staf":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["2"].route;
                break;
            case "kpi_periode":
                url = GeneralData.primaryMenu.personal_menu.sub_menu["2"].route;
                break;
            case "birthday_notification":
                url = GeneralData.primaryMenu.staff_management.sub_menu["1"].route;
                break;
            case "staff_registered":
                url = GeneralData.primaryMenu.staff_management.sub_menu["1"].route;
                break;
            default:
                url = "";
        }

        return '/' + appUriUniq + '/' + url;
    }


}