import React, {Component} from 'react';
import {Badge, Icon, List, Popover, Tabs, Button} from "antd";
import {Link, Redirect} from 'react-router-dom';
import './styles.scss';
import {GetData, PostData} from "../../../../services/api";
import UserNotificationAll from "../UserNotificationAll";

const {TabPane} = Tabs;


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            total: 0,
            visible: false,
            directPage: false,
            directTo: '',
            notification_id: '',
            notification_type: '',
            autoplay: false
        };
    }

    componentWillMount() {
        this.list_data();
    }

    // async componentDidMount() {
    //     try {
    //         // setInterval(async () => {
    //         //     this.list_data_loop();
    //         // }, 600000);
    //     } catch (e) {
    //         // console.log(e);
    //     }
    // }

    render() {

        const notificationContent = (
            <Tabs defaultActiveKey="1" animated={false}>
                <TabPane
                    key={1} tab={
                    <span>
                      <Icon type="bell"/>
                      Notifikasi
                    </span>
                }>
                    <audio ref="audio_tag" src="https://api.coderrocketfuel.com/assets/pomodoro-times-up.mp3"
                           autoPlay={this.state.autoplay}
                           play/>
                    <List
                        footer={
                            <UserNotificationAll
                                notificationClick={this.notificationClick}
                                notificationTotalChange={this.notificationTotalChange}
                            />
                        }
                        size={"small"}
                        itemLayout="horizontal"
                        dataSource={this.state.list}
                        renderItem={item => {

                            let read_status = item.notification_read === 'yes' ? 'notification-row' : 'notification-row not-yet-read';

                            return (
                                <List.Item className={read_status}
                                           onClick={this.notificationClick(item.notification_id, item.notification_type)}>
                                    <List.Item.Meta
                                        title={item.notification_title}
                                        style={{paddingRight: "25px"}}
                                        description={item.notification_description_view}
                                    />
                                    <div className={"notification-time"}>{item.created_at}</div>
                                </List.Item>
                            )
                        }}
                    />
                </TabPane>
            </Tabs>
        );


        return [
            <span onClick={this.notificationOpen()}>
                <Popover placement="bottomRight"
                         content={notificationContent}
                         trigger="click"
                         overlayClassName="header-popover">
                    <span className={"header-menu-right-item"}>
                        <Badge count={this.state.total}>
                            <Icon type="bell"/>
                        </Badge>
                    </span>
                </Popover>
                {
                    this.state.directPage ? <Redirect
                        to={"/notification-redirect/" + this.state.notification_id + '/' + this.state.notification_type}/> : ''
                }
                </span>,
            this.state.autoplay ? this.setState({autoplay: false}) : ''
        ]
    }

    notificationOpen = () => e => {
        const notification_id_see = this.state.list.map((item, key) => {
            if(item.notification_see === 'no') {
                return item.notification_id;
            }
        });

        var notification_id_arr = notification_id_see.filter(function (el) {
            return el != null;
        });

        if(notification_id_arr.length > 0) {
            let data = {
                notification_id: notification_id_arr
            };
            PostData('/notification-see', data).then((result) => {
                // console.log('notification see done');
                // console.log(result.data.data.total);

                this.setState({
                    total: result.data.data.total,
                    list: result.data.data.list
                })
            });
        }
    };


    notificationClick = (notification_id, notification_type) => e => {
        let data = {
            notification_id: notification_id
        };
        PostData('/notification-read', data).then((result) => {

            this.setState({
                directPage: true,
                notification_id: notification_id,
                notification_type: notification_type,
                total: result.data.total
            })
        });
    };

    notificationTotalChange = (total)  => {
        console.log('notificationTotalChange', total);
        this.setState({
            total: total
        })
    };

    list_data = () => {
        GetData('/notification-list').then((result) => {

            this.setState({
                list: result.data.data.list,
                total: result.data.data.total
            })
        })
    };

    list_data_loop = () => {
        GetData('/notification-list').then((result) => {

            let autoplay = false;
            if (result.data.data.total > this.state.total) {
                autoplay = true;
            }

            this.setState({
                autoplay: autoplay,
                list: result.data.data.list,
                total: result.data.data.total
            })
        })
    };


    popoverStatusInverse = e => {
        this.setState({
            visible: !this.state.visible,
        });
    };

    popoverStatusDisable = e => {
        this.setState({
            visible: false,
        });
    };


}

export default Index