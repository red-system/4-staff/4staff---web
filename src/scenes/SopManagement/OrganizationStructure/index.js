import React, {Component} from "react";
import {
    Row, Col, Tabs, Icon
} from 'antd';

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';

import '../../../styles/ui/_card-icon.scss';
import Overview from "./Overview";
import StructureOverview from "./StructureOverview";

import DepartmentData from "./DepartmentDataList";
import {menuActionViewAccess} from "../../../services/app/General";


let pageNow = GENERALDATA.primaryMenu.sop_management.sub_menu["3"];
let pageParent = GENERALDATA.primaryMenu.sop_management;
const { TabPane } = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

class ConfigHrd extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            activeKey:"overview",
            listRefresh: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }


    render() {

        let menuBaganOrganisasi = menuActionViewAccess('sop_management', 'organization_structure', 'bagan_organisasi');
        let menuListDepartment = menuActionViewAccess('sop_management', 'organization_structure', 'list_department');
        let menuListPosition = menuActionViewAccess('sop_management', 'organization_structure', 'list_position');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card">
                                <TabPane className={menuBaganOrganisasi.class} tab={<span><Icon type="check-circle"/> Bagan Organisasi</span>}
                                         key="overview">
                                    <Overview
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </TabPane>
                                <TabPane className={menuListDepartment.class} tab={<span><Icon type="info-circle"/> Departemen</span>}
                                         key="deparment">
                                    <DepartmentData
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}/>
                                </TabPane>
                                <TabPane className={menuListPosition.class} tab={<span><Icon type="radar-chart"/> Posisi</span>}
                                         key="position-2">
                                    <StructureOverview
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun} />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }

}

export default ConfigHrd;