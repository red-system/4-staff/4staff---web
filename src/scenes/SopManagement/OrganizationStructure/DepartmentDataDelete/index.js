import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

class Delete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false
        }
    }


    render() {

        let menuDelete = menuActionViewAccess('sop_management', 'organization_structure', 'delete_department');
        const text = 'Yakin hapus data ini ?';

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button className={menuDelete.class} type="danger" icon={"close"} loading={this.state.isLoading}>
                    Hapus
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        this.setState({
            isLoading: true
        });
        DeleteData(`/department/${props.record.department_id}`)
            .then((result) => {
                props.listRefreshRun(true);
                message.success(result.data.message);
                this.setState({isLoading: false});
            });
    }
}



export default Delete;