import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, message} from "antd";
import {PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            modalVisible: false,
            formError: [],
            formValue: props.record

        };

        this.insert = this.insert.bind(this);
    }

    render() {
        let menuEdit = menuActionViewAccess('sop_management', 'organization_structure', 'edit_department');

        return (
            <span>
                <Button className={menuEdit.class} type="success" onClick={this.modalStatus(true)}>
                    <Icon type="edit"/>
                    Edit
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout} onSubmit={this.insert}>
                        <Form.Item
                            label="Nama Departemen"
                            {...this.state.formError.department_title}>
                            <Input onChange={this.onChange("department_title")} value={this.state.formValue.department_title} />
                        </Form.Item>
                        <Form.Item
                            label="Keterangan"
                            {...this.state.formError.department_description}>
                            <Input onChange={this.onChange("department_description")} value={this.state.formValue.department_description}/>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType="submit" icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </span>
        )
    }

    onChange = name => value => {
        if(typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value
        });
    };

    insert(e) {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        PostData(`/department/${this.props.record.department_id}`, this.state.formValue)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 7},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 17},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};

export default Create;