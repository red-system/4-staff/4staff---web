import React, { Component } from "react";
import { Button, Form, Icon, Input, Modal, message, Select, Table } from "antd";
import { PostData, PutData, GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import Edit from "../../../StaffManagement/StaffPosition/Edit";
import StaffPositionList from "../../../StaffManagement/StaffPosition/List";
import { map, get } from "lodash";
const Search = Input.Search;

export default class List extends Component {
    constructor(props) {
        super(props);

        let { node } = props;

        this.state = {
            isLoading: true,
            data: [],
            structure_id: node.structure_id,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var departmentTitle = false;
            if (get(record, "structure.department.department_title") != null) {
                departmentTitle = get(record, "structure.department.department_title").toString().match(reg);
            }
            var structureName = false;
            if (get(record, "structure.structure_name") != null) {
                structureName = get(record, "structure.structure_name").toString().match(reg);
            }
            if (!staffName && !departmentTitle && !structureName) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let department;
        let menuList = menuActionViewAccess("staff_management", "staff_position", "list");
        let menuEdit = menuActionViewAccess("staff_management", "staff_position", "edit");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", key: "no", width: 40 },
            { title: "Staf", dataIndex: "staff_name", key: "staff_name" },
            { title: "Departemen", dataIndex: "structure.department.department_title", key: "structure.department.department_title" },
            { title: "Posisi", dataIndex: "structure.structure_name", key: "structure.structure_name" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => {
                    return (
                        <Button.Group size={"small"}>
                            <span className={menuEdit.class}>
                                <Edit record={record} listRefreshRun={this.props.listRefreshRun} />
                            </span>
                        </Button.Group>
                    );
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <span>
                <Button type="warning" onClick={this.modalStatus(true)} size={"small"}>
                    <Icon type={"unordered-list"} />
                    List Staff
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="unordered-list" /> List Staff
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <div className="row mb-1">
                        <div className="col-9"></div>
                        <div className="col-3 pull-right">
                            <Search
                                size="default"
                                ref={(ele) => (this.searchText = ele)}
                                suffix={suffix}
                                onChange={this.onSearch}
                                placeholder="Search Records"
                                value={this.state.searchText}
                                onPressEnter={this.onSearch}
                            />
                        </div>
                    </div>
                    <Table className={menuList.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
                </Modal>
            </span>
        );
    }

    list() {
        GetData(`/staff-position/by_structure/${this.state.structure_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 17 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};
