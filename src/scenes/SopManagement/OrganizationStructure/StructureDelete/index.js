import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData} from "../../../../services/api";


class Delete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            node: props.node,
            listRefreshRun: props.listRefreshRun
        }
    }

    render() {
        const {node, listRefreshRun} = this.state;
        const desc = 'Yakin hapus data ini ?';

        function confirm() {
            let structur_id = node.structure_id;
            DeleteData(`/structure/${structur_id}`)
                .then((result) => {
                    if(result.data.status === 'success') {
                        message.success(result.data.message);
                        listRefreshRun(true);
                    } else {
                        message.warning(result.data.message);
                    }
                })

        }

        return (
            <Popconfirm
                placement="rightTop"
                title={desc}
                onConfirm={confirm}
                okText="Yes"
                cancelText="No"
            >
                <Button type="danger" size={"small"}>
                    <Icon type="close"/>
                    Hapus
                </Button>
            </Popconfirm>
        )
    }
}

export default Delete;