import React, { Component } from "react";
import { Button, Form, Icon, Modal, Radio, Select, message } from "antd";
import { PostData, PutData } from "../../../../services/api";

const { Option } = Select;

class Create extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            isLoading: false,
            formError: [],
            staff: props.staff,
            rewardGuide: props.rewardGuide,
            listRefreshRun: props.listRefreshRun,

            reward_id: props.record.key,
            reward_guide_id: props.record.reward_guide_id,
            staff_id: props.record.staff_id,
            reward_publish: props.record.reward_publish,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            rewardGuide: nextProps.rewardGuide,
        });
    }

    render() {
        const { modalVisible, staff, rewardGuide, isLoading } = this.state;

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit" />
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Data
                    </span>
                }
                visible={modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Staff" {...this.state.formError.staff_id}>
                        <Select
                            showSearch
                            placeholder="Pilih Staf"
                            optionFilterProp="children"
                            defaultValue={this.props.record.staff_id}
                            key={this.props.record.staff_id}
                            onChange={this.onChange("staff_id")}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {staff.map((item, key) => {
                                return (
                                    <Option value={item["staff_id"]} key={key}>
                                        {item["staff_name"]}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Kinerja" {...this.state.formError.reward_guide_id}>
                        <Select
                            showSearch
                            placeholder="Pilih Kinerja"
                            optionFilterProp="children"
                            defaultValue={this.state.reward_guide_id}
                            onChange={this.onChange("reward_guide_id")}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {rewardGuide.map((item, key) => {
                                return (
                                    <Option value={item["reward_guide_id"]} key={key}>
                                        {item["rewardg_title"]}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Publish" {...this.state.formError.reward_publish}>
                        <Radio.Group defaultValue={this.state.reward_publish} onChange={this.onChange("reward_publish")} buttonStyle="solid">
                            <Radio.Button value="yes">Ya</Radio.Button>
                            <Radio.Button value="no">Tidak</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={isLoading} onClick={this.update}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            [name]: value,
        });
    };

    update = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });
        PostData("/reward/" + this.state.reward_id, this.state).then((result) => {
            const data = result.data;

            if (data.status === "success") {
                message.success(data.message);
                this.props.listRefreshRun(true);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
    },
};

export default Create;
