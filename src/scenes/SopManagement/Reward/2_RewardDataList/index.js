import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import Edit from "../2_RewardDataEdit";
import Delete from "../2_RewardDataDelete";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listRefresh: props.listRefresh,
            listRefreshRun: props.listRefreshRun,
            staff: props.listRefreshRun,
            rewardGuide: props.rewardGuide,
            isLoading: true,

            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            listRefreshRun: nextProps.listRefreshRun,
            staff: nextProps.staff,
            rewardGuide: nextProps.rewardGuide,
        });
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var rewardTitle = false;
            if (get(record, "rewardg_title") != null) {
                rewardTitle = get(record, "rewardg_title").toString().match(reg);
            }
            var rewardDescription = false;
            if (get(record, "rewardg_description") != null) {
                rewardDescription = get(record, "rewardg_description").toString().match(reg);
            }
            var updatedAt = false;
            if (get(record, "updated_at") != null) {
                updatedAt = get(record, "updated_at").toString().match(reg);
            }
            if (!staffName && !rewardTitle && !rewardDescription && !updatedAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("sop_management", "reward", "list_reward_staff");
        let menuEdit = menuActionViewAccess("sop_management", "reward", "edit_reward_staff");
        let menuDelete = menuActionViewAccess("sop_management", "reward", "delete_reward_staff");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Nama Staff", dataIndex: "staff_name", key: "staff_name" },
            { title: "Kinerja", dataIndex: "rewardg_title", key: "rewardg_title" },
            { title: "Penghargaan", dataIndex: "rewardg_description", key: "rewardg_description" },
            {
                title: "Publish",
                dataIndex: "reward_publish",
                key: "reward_publish",
                render: (text, record) => {
                    return status_view(record.reward_publish);
                },
            },
            { title: "Waktu Pembaruan", dataIndex: "updated_at", key: "updated_at" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit
                                formLayout={formLayout}
                                text={text}
                                record={record}
                                staff={this.state.staff}
                                listRefreshRun={this.state.listRefreshRun}
                                rewardGuide={this.state.rewardGuide}
                            />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete text={text} record={record} listRefreshRun={this.state.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: item["reward_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={data} size="small" bordered={true} loading={this.state.isLoading} />;
            </div>
        );
    }

    list() {
        GetData("/reward").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 18,
        offset: 6,
    },
    lg: {
        span: 18,
        offset: 6,
    },
};

export default RewardGuide;
