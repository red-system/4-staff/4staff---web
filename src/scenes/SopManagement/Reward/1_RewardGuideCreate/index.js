import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Row, Col, Radio, message} from "antd";
import {PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout,
            isLoading: false,
            formError: [],

            rewardg_title: '', rewardg_description: '', rewardg_publish: 'yes'
        };

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('ref', this.inputNode);
    }

    render() {

        const {modalVisible, isLoading} = this.state;
        const {rewardg_title, rewardg_description, rewardg_publish} = this.state;
        const {rewardg_title_error, rewardg_description_error, rewardg_publish_error} = this.state.formError;
        let menuCreate = menuActionViewAccess('sop_management', 'reward', 'create_reward');

        return (
            <div className={menuCreate.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout} className={"form-send"}>
                        <Form.Item
                            label="Kinerja"
                            {...rewardg_title_error}>
                            <Input defaultValue={rewardg_title} onChange={this.onChange('rewardg_title')}/>
                        </Form.Item>
                        <Form.Item
                            label="Penghargaan"
                            {...rewardg_description_error}>
                            <Input defaultValue={rewardg_description} onChange={this.onChange('rewardg_description')}/>
                        </Form.Item>
                        <Form.Item
                            label="Publish"
                            {...rewardg_publish_error}>
                            <Radio.Group defaultValue={rewardg_publish} onChange={this.onChange('rewardg_publish')}
                                         buttonStyle="solid">
                                <Radio.Button value="yes">Ya</Radio.Button>
                                <Radio.Button value="no">Tidak</Radio.Button>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={isLoading}
                                    onClick={this.insert}
                            >
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            [name]: value
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        let data = {
            rewardg_title: this.state.rewardg_title,
            rewardg_description: this.state.rewardg_description,
            rewardg_publish: this.state.rewardg_publish,
        };

        PostData('/reward-guide', data)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}_error`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }

}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;