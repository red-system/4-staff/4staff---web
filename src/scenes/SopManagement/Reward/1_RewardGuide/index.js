import React, {Component} from 'react';

import Create from '../1_RewardGuideCreate';
import List from '../1_RewardGuideList';

class RewardGuide extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listRefresh: props.listRefresh,
            listRefreshRun: props.listRefreshRun
        };
    }

    render() {

        const props = {
            listRefresh: this.state.listRefresh,
            listRefreshRun: this.state.listRefreshRun,
            formLayout: formLayout
        };

        return (
            <div>
                <Create {...props}/>
                <List {...props} />
            </div>
        )
    }

}



const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 18,
        offset: 6
    },
    'lg': {
        span: 18,
        offset: 6
    }
};

export default RewardGuide;