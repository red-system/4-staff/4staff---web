import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import Create from "../2_PunishmentDataCreate";
import Edit from "../2_PunishmentDataEdit";
import Delete from "../2_PunishmentDataDelete";
import { GetData } from "../../../../services/api";
import { dateFormatterApp, menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class Punishment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listRefresh: props.listRefresh,
            listRefreshRun: props.listRefreshRun,
            dataPunishment: [],
            dataStaff: [],
            dataPunishmentGuide: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataPunishment, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var punishmentDate = false;
            if (get(record, "punishment_date_format") != null) {
                punishmentDate = get(record, "punishment_date_format").toString().match(reg);
            }
            var punishmentTitle = false;
            if (get(record, "punishmentg_title") != null) {
                punishmentTitle = get(record, "punishmentg_title").toString().match(reg);
            }
            var punishmentDescription = false;
            if (get(record, "punishmentg_description") != null) {
                punishmentDescription = get(record, "punishmentg_description").toString().match(reg);
            }
            if (!staffName && !punishmentDate && !punishmentTitle && !punishmentDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.dataPunishment,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.dataPunishment,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("sop_management", "punishment", "list_punishment_staff");
        let menuCreate = menuActionViewAccess("sop_management", "punishment", "create_punishment_staff");
        let menuEdit = menuActionViewAccess("sop_management", "punishment", "edit_punishment_staff");
        let menuDelete = menuActionViewAccess("sop_management", "punishment", "delete_punishment_staff");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Staff", dataIndex: "staff_name", key: "staff_name" },
            { title: "Tanggal", dataIndex: "punishment_date_format", key: "punishment_date_format" },
            { title: "Judul Pelanggaran", dataIndex: "punishmentg_title", key: "punishmentg_title" },
            { title: "Keterangan Pelanggaran", dataIndex: "punishmentg_description", key: "punishmentg_description" },
            {
                title: "Publish",
                dataIndex: "punishment_publish",
                key: "punishment_publish",
                render: (text, record) => {
                    return status_view(record.punishment_publish);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} {...this.state} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} {...this.state} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["punishment_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <span className={menuCreate.class}>
                    <Create {...this.state} />
                </span>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} loading={this.state.isLoading} bordered={true} dataSource={dataList} size="small" />
            </div>
        );
    }

    async list() {
        await GetData("/punishment").then((result) => {
            this.setState({
                dataPunishment: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
        await GetData("/punishment-guide").then((result) => {
            this.setState({
                dataPunishmentGuide: result.data.data,
                isLoading: false,
            });
        });
        await GetData("/staff").then((result) => {
            this.setState({
                dataStaff: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default Punishment;
