import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Row, Col, message} from "antd";
import {PostData} from "../../../../services/api";
import {menuActionViewAccess} from "../../../../services/app/General";

const {TextArea} = Input;

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formLayout: props.formLayout,
            formError: [
                ...form_fields
            ],

            ...fields,

        };

        this.modalStatus = this.modalStatus.bind(this);
        this.insert = this.insert.bind(this);
        this.onChangeFile = this.onChangeFile.bind(this);
    }

    render() {
        const {modalVisible, formLayout, isLoading} = this.state;
        const {workf_title, workf_description, workf_filename} = this.state;
        const {workf_title_field, workf_description_field, workf_filename_field} = this.state.formError;
        let menuCreate = menuActionViewAccess('sop_management', 'workflow', 'create');

        return (
            <div className={menuCreate.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Judul"
                            {...workf_title_field}>
                            <Input defaultValue={workf_title} onChange={this.onChange('workf_title')}/>
                        </Form.Item>
                        <Form.Item
                            label="Deksripsi"
                            {...workf_description_field}>
                            <TextArea defaultValue={workf_description} onChange={this.onChange('workf_description')}/>
                        </Form.Item>
                        <Form.Item
                            label="File Deksripsi Pekerjaan"
                            {...workf_filename_field}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.onChangeFile}/>
                            </div>
                        </Form.Item>

                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType="submit" icon={"check"} loading={isLoading}
                                    onClick={this.insert}>
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value
        });
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            [name]: value
        });
    };

    onChangeFile(event) {
        this.setState({
            workf_filename: event.target.files[0]
        })
    }

    insert(e) {
        e.preventDefault();
        this.setState({
            isLoading: true
        })

        const {workf_title, workf_description, workf_filename} = this.state;

        const formData = new FormData();
        formData.append('workf_title', workf_title);
        formData.append('workf_description', workf_description);
        formData.append('workf_filename', workf_filename);

        PostData('/work-flow', formData)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                        formError: [],

                        ...fields
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}_field`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

const fields = {
    workf_title: '',
    workf_description: '',
    workf_filename: '',
}

const form_fields = Object.keys(fields).map(function (item, i) {
    return `${item}_field`;
});

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 7},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 17},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};

export default Create;