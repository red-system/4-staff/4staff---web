import React, { Component } from "react";
import { Row, Col, Button, Form, Radio, InputNumber, Select, Spin, message } from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";
import { Link } from "react-router-dom";
import { GetData, PostData } from "../../../services/api";

import "../../../styles/ui/_card-icon.scss";
import { menuActionViewAccess } from "../../../services/app/General";

const FormItem = Form.Item;
const { Option } = Select;

let pageNow = GENERALDATA.primaryMenu.sop_management.sub_menu["1"];
let pageParent = GENERALDATA.primaryMenu.sop_management;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

const formItemLayout_radius = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 3 },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 12,
            offset: 8,
        },
    },
};

class ConfigHrd extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            data: [],
            isLoading: true,
            leave_quota: 0,
            sum_annual_leave: 0,
            leave_accumulation: 0,
            absence_radius: 0,
            absence_on_break: 0,
            timezone: 0,
            form_error: {},
        };

        this.onChange1 = this.onChange1.bind(this);
        this.onChange2 = this.onChange2.bind(this);
        this.update = this.update.bind(this);
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
        GetData("/hrd-config").then((result) => {
            let data = result.data;
            this.setState({
                leave_quota: data.data[0]["config_value"],
                sum_annual_leave: data.data[1]["config_value"],
                leave_accumulation: data.data[2]["config_value"],
                absence_radius: data.data[3]["config_value"],
                absence_on_break: data.data[4]["config_value"],
                timezone: data.data[5]["config_value"],
                isLoading: false,
            });
        });
    }

    render() {
        const { leave_quota, sum_annual_leave, leave_accumulation, absence_radius, absence_on_break, timezone, isLoading } = this.state;

        let menuList = menuActionViewAccess("sop_management", "hrd_config", "list");
        let menuUpdate = menuActionViewAccess("sop_management", "hrd_config", "update");

        let { leave_quota_field, sum_annual_leave_field, leave_accumulation_field, absence_radius_field, absence_on_break_field, timezone_field } =
            this.state.form_error;

        return (
            <Spin tip={"Loading ..."} spinning={isLoading} className={menuList.class}>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <Form onSubmit={this.update}>
                                    <FormItem {...formItemLayout} {...leave_quota_field} label="Kuota Cuti" required={true}>
                                        <InputNumber
                                            onChange={this.onChange1("leave_quota")}
                                            key={"leave_quota"}
                                            name={"leave_quota"}
                                            value={leave_quota}
                                            formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                                        />
                                    </FormItem>
                                    <FormItem {...formItemLayout} {...sum_annual_leave_field} label="Kalkulasi Cuti ke Tahun Selanjutnya" required={true}>
                                        <Radio.Group
                                            defaultValue={sum_annual_leave}
                                            onChange={this.onChange2("sum_annual_leave")}
                                            buttonStyle="solid"
                                            key={sum_annual_leave}
                                            name={"sum_annual_leave"}
                                        >
                                            <Radio.Button value="yes">Ya</Radio.Button>
                                            <Radio.Button value="no">Tidak</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>
                                    {/*<FormItem*/}
                                    {/*    {...formItemLayout}*/}
                                    {/*    {...leave_accumulation_field}*/}
                                    {/*    label="Sum Anual Leave When Sum Annual Leave is No"*/}
                                    {/*>*/}
                                    {/*    <Radio.Group defaultValue={leave_accumulation}*/}
                                    {/*                 onChange={this.onChange2("leave_accumulation")}*/}
                                    {/*                 buttonStyle="solid"*/}
                                    {/*                 key={leave_accumulation}*/}
                                    {/*                 name={"leave_accumulation"}>*/}
                                    {/*        <Radio.Button value="yes">Ya</Radio.Button>*/}
                                    {/*        <Radio.Button value="no">Tidak</Radio.Button>*/}
                                    {/*    </Radio.Group>*/}
                                    {/*</FormItem>*/}
                                    <FormItem {...formItemLayout_radius} {...absence_radius_field} label="Radius Absensi (meter)" required={true}>
                                        <InputNumber
                                            onChange={this.onChange1("absence_radius")}
                                            key={"absence_radius"}
                                            value={absence_radius}
                                            formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                                        />
                                    </FormItem>
                                    <FormItem {...formItemLayout} {...absence_on_break_field} label="Absen saat Istirahat" required={true}>
                                        <Radio.Group
                                            defaultValue={absence_on_break}
                                            buttonStyle="solid"
                                            key={absence_on_break}
                                            onChange={this.onChange2("absence_on_break")}
                                            name={"absence_on_break"}
                                        >
                                            <Radio.Button value="yes">Ya</Radio.Button>
                                            <Radio.Button value="no">Tidak</Radio.Button>
                                        </Radio.Group>
                                    </FormItem>
                                    <FormItem {...formItemLayout} {...timezone_field} label="Zona Waktu" required={true}>
                                        <Select
                                            showSearch
                                            key={timezone}
                                            defaultValue={timezone}
                                            style={{ width: 200 }}
                                            name={"timezone"}
                                            placeholder="Pilih Zona Waktu"
                                            optionFilterProp="children"
                                            onChange={this.onChange1("timezone")}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            <Option value="wita">WITA</Option>
                                            <Option value="wib">WIB</Option>
                                            <Option value="wit">WIT</Option>
                                        </Select>
                                    </FormItem>
                                    <Form.Item {...tailFormItemLayout}>
                                        <Button
                                            className={menuUpdate.class}
                                            type="primary"
                                            htmlType="submit"
                                            icon={"check"}
                                            onClick={this.update}
                                            loading={isLoading}
                                        >
                                            Perbarui Data
                                        </Button>
                                        &nbsp;
                                        <Link to={`../${pageParent.route}`}>
                                            <Button type="warning" htmlType="submit" icon={"rollback"}>
                                                Kembali
                                            </Button>
                                        </Link>
                                    </Form.Item>
                                </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Spin>
        );
    }

    onChange1 = (name) => (value) => {
        this.setState({
            [name]: value,
        });
    };

    onChange2 = (name) => (e) => {
        console.log("this is rerendeting too");
        this.setState({
            [name]: e.target.value,
        });
    };

    update(e) {
        e.preventDefault();
        const { leave_quota, sum_annual_leave, leave_accumulation, absence_radius, absence_on_break, timezone } = this.state;
        this.setState({
            isLoading: true,
            form_error: {},
        });

        const data = {
            leave_quota: leave_quota,
            sum_annual_leave: sum_annual_leave,
            leave_accumulation: leave_accumulation,
            absence_radius: absence_radius,
            absence_on_break: absence_on_break,
            timezone: timezone,
        };

        PostData("/hrd-config", data).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                });
            } else {
                let errors = data.errors;
                let form_error = [];

                Object.keys(errors).map(function (key) {
                    form_error[`${key}_field`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    form_error: form_error,
                    isLoading: false,
                });
            }
        });

        return false;
    }
}

export default ConfigHrd;
