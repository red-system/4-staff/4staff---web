import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Edit from "../Edit";
import Delete from "../Delete";
import ViewFile from "../ViewFile";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RulesRegulation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            // var staffName = false;
            // if (get(record, "staff_name") != null) {
            //     staffName = get(record, "staff_name").toString().match(reg);
            // }
            // var punishmentDate = false;
            // if (get(record, "punishment_date_format") != null) {
            //     punishmentDate = get(record, "punishment_date_format").toString().match(reg);
            // }
            var rulesTitle = false;
            if (get(record, "rulesr_title") != null) {
                rulesTitle = get(record, "rulesr_title").toString().match(reg);
            }
            var rulesDescription = false;
            if (get(record, "rulesr_description") != null) {
                rulesDescription = get(record, "rulesr_description").toString().match(reg);
            }
            var updatedAt = false;
            if (get(record, "updated_at") != null) {
                updatedAt = get(record, "updated_at").toString().match(reg);
            }
            if (!rulesTitle && !rulesDescription && !updatedAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuViewFile = menuActionViewAccess("sop_management", "rules_regulation", "view_file");
        let menuEdit = menuActionViewAccess("sop_management", "rules_regulation", "edit");
        let menuDelete = menuActionViewAccess("sop_management", "rules_regulation", "delete");
        let menuList = menuActionViewAccess("sop_management", "rules_regulation", "list");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Judul", dataIndex: "rulesr_title", key: "rulesr_title" },
            { title: "Deskripsi", dataIndex: "rulesr_description", key: "rulesr_description" },
            { title: "Waktu Pembaruan", dataIndex: "updated_at", key: "updated_at" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 250,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuViewFile.class}>
                            <ViewFile record={record} />
                        </span>
                        <span className={menuEdit.class}>
                            <Edit record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={dataList} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/rules-regulation").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RulesRegulation;
