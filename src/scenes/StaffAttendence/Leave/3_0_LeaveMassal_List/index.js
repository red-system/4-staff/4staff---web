import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Create from "../3_0_LeaveMassal_Create";
import Edit from "../3_0_LeaveMassal_Edit";
import Delete from "../3_0_LeaveMassal_Delete";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor() {
        super();
        this.state = {
            listRefresh: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var leaveMassiveTitle = false;
            if (get(record, "leave_massive_title") != null) {
                leaveMassiveTitle = get(record, "leave_massive_title").toString().match(reg);
            }
            var leaveMassiveDate = false;
            if (get(record, "leave_massive_date_format") != null) {
                leaveMassiveDate = get(record, "leave_massive_date_format").toString().match(reg);
            }
            var leaveMassiveDescription = false;
            if (get(record, "leave_massive_description") != null) {
                leaveMassiveDescription = get(record, "leave_massive_description").toString().match(reg);
            }
            if (!leaveMassiveTitle && !leaveMassiveDate && !leaveMassiveDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_attendence", "leave", "list_leave");
        let menuCreate = menuActionViewAccess("staff_attendence", "leave", "create_leave");
        let menuEdit = menuActionViewAccess("staff_attendence", "leave", "edit_leave");
        let menuDelete = menuActionViewAccess("staff_attendence", "leave", "delete_leave");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Nama Leave Masal",
                dataIndex: "leave_massive_title",
                key: "leave_massive_title",
            },
            {
                title: "Tanggal Leave Masal",
                dataIndex: "leave_massive_date_format",
                key: "leave_massive_date_format",
            },
            {
                title: "Keterangan Leave Masal",
                dataIndex: "leave_massive_description",
                key: "leave_massive_description",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 200,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} listRefreshRun={this.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefreshRun={this.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <span className={menuCreate.class}>
                    <Create listRefresh={this.state.listRefresh} listRefreshRun={this.listRefreshRun} />
                </span>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} bordered={true} dataSource={data} size="small" />
            </div>
        );
    }

    list = () => {
        GetData("/leave-massive").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                listRefresh: false,
            });
        });
    };

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });
    };
}

export default RewardGuide;
