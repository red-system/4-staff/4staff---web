import React, { Component } from "react";
import { DatePicker, Form, Button, Select, message, InputNumber } from "antd";

import LeaveKuotaEdit from "../2_0_LeaveKuota_Edit";
import { GetData, PostData, PutData, CancelFetch } from "../../../../services/api";
import { menuActionViewAccess, strRandom, yearToDay } from "../../../../services/app/General";

const { Option } = Select;

export default class AppComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isLoadingDepartment: false,
            isLoadingPosition: false,
            isLoadingStaff: false,
            isLoadingYear: false,
            isLoadingButton: false,
            listRefresh: false,

            dataDepartment: [],
            dataPosition: [],
            dataStaff: [],

            dataYears: [],
            dataLeaveQuota: [],
            yearTotal: 0,

            formError: [],
            formValue: {
                department_id: "all",
                structure_id: "all",
                staff_id: ["all"],
                year: yearToDay(),
            },
        };
    }

    componentWillMount() {
        this.dataDepartment();
        this.dataStructure();
        this.dataStaff();
    }

    componentDidMount() {
        this.leave_quota();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.listRefresh) {
            this.leave_quota_re();
        }
    }

    render() {
        let menuList = menuActionViewAccess("staff_attendence", "leave", "list_kuota");
        let menuEdit = menuActionViewAccess("staff_attendence", "leave", "edit_kuota");

        return (
            <div className={menuList.class}>
                <Form {...formItemLayout}>
                    <Form.Item label="Departemen" {...this.state.formError.department_id}>
                        <Select
                            showSearch
                            placeholder="Pilih Departemen"
                            optionFilterProp="children"
                            value={this.state.formValue.department_id}
                            onChange={this.onChangeDepartment()}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            disabled={this.state.isLoadingDepartment}
                        >
                            <Option key={"all"} value={"all"}>
                                Semua Departemen
                            </Option>
                            {this.state.dataDepartment.map((item, key) => {
                                return (
                                    <Option key={key} value={item["department_id"]}>
                                        {item["department_title"]}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Posisi" {...this.state.formError.structure_id}>
                        <Select
                            showSearch
                            placeholder="Pilih Posisi"
                            optionFilterProp="children"
                            loading={this.state.isLoadingPosition}
                            onChange={this.onChangePosition()}
                            value={this.state.formValue.structure_id}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            disabled={this.state.isLoadingPosition}
                        >
                            <Option key={"all"} value={"all"}>
                                Semua Posisi
                            </Option>
                            {this.state.dataPosition.map((item, key) => {
                                return (
                                    <Option key={key} value={item["structure_id"]}>
                                        {item["structure_name"]}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Staff" {...this.state.formError.staff_id}>
                        <Select
                            showSearch
                            mode="multiple"
                            placeholder="Pilih Staff"
                            optionFilterProp="children"
                            loading={this.state.isLoadingStaff}
                            onChange={this.onChangeStaff()}
                            value={this.state.formValue.staff_id}
                            onSelect={this.onSelectStaff()}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            disabled={this.state.isLoadingStaff}
                        >
                            <Option key={"all"} value={"all"}>
                                Semua Staff
                            </Option>
                            {this.state.dataStaff.map((item, key) => {
                                return (
                                    <Option key={key} value={item["staff_id"]}>
                                        {item["staff_name"]}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Tahun Kuota Cuti" {...this.state.formError.year}>
                        <InputNumber defaultValue={this.state.formValue.year} onChange={this.onChange("year")} disabled={this.state.isLoadingYear} />
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" icon={"eye"} onClick={this.leave_quota()} disabled={this.state.isLoadingButton}>
                            Lihat Data
                        </Button>
                    </Form.Item>
                </Form>
                <hr />

                <table className="table-bordered">
                    <thead>
                        <tr>
                            <th rowSpan={this.state.yearTotal}>Name</th>
                            <th rowSpan={this.state.yearTotal}>Position</th>
                            {this.state.dataYears.map((item, key) => {
                                return (
                                    <th key={key} colSpan="2" className={"text-center"}>
                                        {item["leave_quota_year"]}
                                    </th>
                                );
                            })}
                            <th rowSpan={this.state.yearTotal} className={"text-center"} width={"60"}>
                                Action
                            </th>
                        </tr>
                        <tr>
                            {this.state.dataYears.map((item, key) => {
                                return [
                                    <th key={0} className={"text-center"}>
                                        Quota
                                    </th>,
                                    <th key={1} className={"text-center"}>
                                        Used
                                    </th>,
                                ];
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataLeaveQuota.map((item, key) => {
                            let quote = {};
                            return (
                                <tr key={key}>
                                    <td>{item["staff_name"]}</td>
                                    <td>{item["position"]["structure_name"]}</td>
                                    {this.state.dataYears.map((item2, key2) => {
                                        let year = item2["leave_quota_year"];
                                        let leave_quota_annual = item["quote"][year]["leave_quota_annual"];
                                        let leave_quota_used = item["quote"][year]["leave_quota_used"];
                                        let leave_quota_id = item["quote"][year]["leave_quota_id"];
                                        let staff_id = item["staff_id"];
                                        quote = {
                                            ...quote,
                                            [year]: {
                                                leave_quota_annual: leave_quota_annual,
                                                leave_quota_used: leave_quota_used,
                                                leave_quota_id: leave_quota_id,
                                                staff_id: staff_id,
                                            },
                                        };
                                        return [
                                            <th key={key2} className={"text-center"}>
                                                {leave_quota_annual}
                                            </th>,
                                            <th key={key2} className={"text-center"}>
                                                {leave_quota_used}
                                            </th>,
                                        ];
                                    })}
                                    <td>
                                        <span className={menuEdit.class}>
                                            <LeaveKuotaEdit
                                                record={item}
                                                staff_id={item["staff_id"]}
                                                data_years={this.state.dataYears}
                                                quote={quote}
                                                listRefreshRun={this.listRefreshRun}
                                            />
                                        </span>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

    onChange = (field) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [field]: value,
            },
        });
    };

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });
    };

    onChangeDepartment = () => (department_id) => {
        this.setState({
            isLoadingPosition: true,
            isLoadingStaff: true,
            isLoadingButton: true,
        });

        let data = {
            department_id: department_id,
        };
        PostData("/structure/department", data).then((result) => {
            this.setState({
                dataPosition: result.data.data,
                formValue: {
                    ...this.state.formValue,
                    department_id: department_id,
                    structure_id: "all",
                    staff_id: ["all"],
                },
            });
        });

        GetData("/structure/department/" + department_id + "/staff").then((result) => {
            this.setState({
                isLoadingPosition: false,
                isLoadingStaff: false,
                isLoadingButton: false,
                dataStaff: result.data.data,
            });
        });
    };

    onChangePosition = () => (structure_id) => {
        this.setState({
            isLoadingPosition: true,
            isLoadingStaff: true,
            isLoadingButton: true,
            isLoadingYear: true,
            isLoading: true,
        });

        let data = {
            structure_id: structure_id,
            department_id: this.state.formValue.department_id,
        };

        PostData("/structure/position/staff", data).then((result) => {
            this.setState({
                dataStaff: result.data.data,
                isLoadingPosition: false,
                isLoadingStaff: false,
                isLoadingButton: false,
                isLoadingYear: false,
                isLoading: false,
                formValue: {
                    ...this.state.formValue,
                    structure_id: structure_id,
                    staff_id: ["all"],
                },
            });
        });
    };

    onSelectStaff = () => (staff_id) => {
        if (staff_id === "all") {
            this.setState({
                formValue: {
                    ...this.state.formValue,
                    staff_id: ["all"],
                },
            });
        }
    };

    onChangeStaff = () => (staff_id) => {
        console.log(staff_id);

        let exist_all = false;
        let staff_id_count = staff_id.length;
        staff_id.map((item, key) => {
            if (item === "all") {
                exist_all = true;
                return false;
            }
        });

        console.log(exist_all, staff_id_count);

        if (exist_all) {
            let staff_id_new = [];
            staff_id.map((item, key) => {
                if (item !== "all") {
                    staff_id_new[key] = item;
                }
            });

            staff_id = staff_id_new;
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_id: staff_id,
            },
        });
    };

    dataDepartment = () => {
        GetData("/department").then((result) => {
            this.setState({
                dataDepartment: result.data.data,
            });
        });
    };

    dataStructure = () => {
        GetData("/structure").then((result) => {
            this.setState({
                dataPosition: result.data.data,
            });
        });
    };

    dataStaff = () => {
        GetData("/staff").then((result) => {
            this.setState({
                dataStaff: result.data.data,
            });
        });
    };

    leave_quota = () => (e) => {
        this.setState({
            isLoadingDepartment: true,
            isLoadingPosition: true,
            isLoadingStaff: true,
            isLoadingYear: true,
            isLoadingButton: true,
            isLoading: true,
        });

        PostData(`/leave-quote`, this.state.formValue).then((result) => {
            const data = result.data;

            this.setState({
                isLoadingDepartment: false,
                isLoadingPosition: false,
                isLoadingStaff: false,
                isLoadingYear: false,
                isLoadingButton: false,
                isLoading: false,
            });

            if (data.status === "success") {
                message.success(data.message);
                this.setState({
                    dataLeaveQuota: data.data.staff,
                    dataYears: data.data.years,
                    yearTotal: data.data.year_total,

                    formError: [],
                    listRefresh: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };

    leave_quota_re() {
        this.setState({
            isLoading: true,
        });

        PostData(`/leave-quote`, this.state.formValue).then((result) => {
            const data = result.data;

            if (data.status === "success") {
                message.success(data.message);
                this.setState({
                    dataLeaveQuota: data.data.staff,
                    dataYears: data.data.years,
                    yearTotal: data.data.year_total,

                    formError: [],
                    listRefresh: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    }
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 4,
        },
    },
};
