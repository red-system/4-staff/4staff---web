import React, {Component} from 'react';
import {Button, Col, Form, Icon, Modal, Row} from "antd";
import {status_view} from "../../../../services/app/General";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formLayout: props.formLayout
        };
    }

    render() {

        const {record} = this.props;

        return [
            <Button type={"primary"} onClick={this.modalStatus(true)}>
                <Icon type="eye"/> Detail
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="info-circle"/> Detil Manajemen Cuti
                        </span>
                }
                visible={this.state.modalVisible}
                width={600}
                onCancel={this.modalStatus(false)}
                footer={[
                    <div className={"text-center"}>
                        <Button key="back" type="warning" onClick={this.modalStatus(false)}>
                            <Icon type="close"/> Tutup
                        </Button>
                    </div>
                ]}
            >
                <Form {...formItemLayout} className={"form-info"}>
                    <Form.Item
                        label="Nama Cuti/Ijin">
                        {record.leavem_name}
                    </Form.Item>
                    <Form.Item
                        label="Status Pembayaran Cuti/Ijin">
                        {status_view(record.leavem_regulation)}
                    </Form.Item>
                    <Form.Item
                        label="Dukungan File">
                        {status_view(record.leavem_attachment)}
                    </Form.Item>
                    {/*<Form.Item*/}
                    {/*    label="Rekomendasi Kuota">*/}
                    {/*    {record.leavem_quota_recomendation}*/}
                    {/*</Form.Item>*/}
                    <Form.Item
                        label="Perlu Pengganti">
                        {status_view(record.leavem_need_replace)}
                    </Form.Item>
                    <Form.Item
                        label="Status Pengurangan Kuota Cuti/Ijin">
                        {status_view(record.leavem_reduce_quota_status)}
                    </Form.Item>
                    {
                        record.leavem_reduce_quota_status === 'custom' ?
                            <Form.Item
                                label="Jumlah Pengurangan Kuota Cuti/Ijin">
                                {record.leavem_reduce_quota_value} Hari
                            </Form.Item> : ''
                    }
                    <Form.Item
                        label="Minimal Hari pengajuan Cuti/Ijin">
                        {record.leavem_min_request_date} Hari
                    </Form.Item>
                    <Form.Item
                        label="Perlu Maksimal Durasi Cuti/Ijin">
                        {status_view(record.leavem_need_max_duration)}
                    </Form.Item>
                    <Form.Item
                        label="Jumlah Maksimal Durasi Cuti/Ijin">
                        {record.leavem_max_duration} Hari
                    </Form.Item>
                </Form>
            </Modal>
        ]
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 11},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 13},
    },
};