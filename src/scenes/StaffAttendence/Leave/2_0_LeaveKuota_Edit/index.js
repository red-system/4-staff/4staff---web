import React, {Component} from 'react';
import {Button, Col, Form, Icon, Modal, Row, InputNumber, Input, message} from "antd";
import {PostData, PutData} from "../../../../services/api";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formValue: [],
            formValueData: [],
        };
    }

    componentWillMount(nextProps, nextContext) {
        const {quote} = this.props;
        const data = [];

        Object.keys(this.props.quote).map((year, key) => {
            let leave_quota_year = year;
            let leave_quota_annual = quote[year].leave_quota_annual;
            let leave_quota_used = quote[year].leave_quota_used;
            let leave_quota_id = quote[year].leave_quota_id;
            let staff_id = quote[year].staff_id;

            data[leave_quota_year] = {
                leave_quota_annual: leave_quota_annual,
                leave_quota_used: leave_quota_used,
                leave_quota_id: leave_quota_id,
                leave_quota_year: leave_quota_year,
                staff_id: staff_id,
            };
        });

        //console.log('will mount', data);

        this.setState({
            formValue: data,
            formValueData: data
        })

    }

    render() {

        const {formValue, formValueData} = this.state;

        return (
            <span>
                <Button type="success" onClick={this.modalStatus(true)}>
                    <Icon type="edit"/>
                    Edit
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="edit"/> Edit Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                   <Form {...formItemLayout}>
                        <Form.Item
                            label="Nama Staf">
                            {this.props.record.staff_name}
                        </Form.Item>
                        <Form.Item
                            label="Posisi">
                            {this.props.record.position.structure_name}
                        </Form.Item>
                       {
                           Object.keys(formValueData).map((year, key) => {
                               let leave_quota_id = formValue[year]['leave_quota_id'];
                               let leave_quota_annual = formValue[year]['leave_quota_annual'];
                               let leave_quota_used = formValue[year]['leave_quota_used'];
                               let leave_quota_year = formValue[year]['leave_quota_year'];
                               // console.log(leave_quota_id, formValue);
                               return (
                                   <div key={year+leave_quota_id}>
                                       <strong>Leave Kuota ({leave_quota_year})</strong>
                                       <Form.Item label="Kuota Tahunan">
                                           <Row>
                                               <Col {...divide21}>
                                                   <InputNumber
                                                       defaultValue={leave_quota_annual}
                                                       onChange={this.onChange('leave_quota_annual', leave_quota_year)} />
                                               </Col>
                                               <Col {...divide22} style={{paddingLeft: "20px"}}>
                                                   <Form.Item label="Kuota Digunakan" {...formItemLayout2}>
                                                       <InputNumber
                                                           defaultValue={leave_quota_used}
                                                           onChange={this.onChange('leave_quota_used', leave_quota_year)}/>
                                                   </Form.Item>
                                               </Col>
                                           </Row>
                                       </Form.Item>
                                   </div>
                               )
                           })
                       }

                       <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType="submit" icon={"check"}
                                    loading={this.state.isLoading} onClick={this.update}>
                                Simpan
                            </Button>
                           &nbsp;
                           <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </span>
        )
    }


    onChange = (field, leave_quota_year) => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [leave_quota_year]: {
                    ...this.state.formValue[leave_quota_year],
                    [field]: value
                }
            }
        });


        console.log('onchange', this.state.formValue);
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };

    update = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });
        let data = {
            staff_id: this.props.staff_id,
            data: this.state.formValue,
            data_years: this.props.data_years
        };

        PostData(`/leave-quote/update`, data)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.setState({
                        isLoading: false,
                        modalVisible: false
                    });
                } else {
                    message.warning(data.message);
                    this.setState({
                        isLoading: false
                    });
                }
            });

        return false;
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formItemLayout2 = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 10},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};

const divide21 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 8,
    xl: 8
};

const divide22 = {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 16,
    xl: 16
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;