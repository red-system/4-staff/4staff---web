import React, {Component} from 'react';
import {Button, Form, Icon, Modal} from "antd";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        };
    }

    render() {

        return [
            <Button type={"primary"} onClick={this.modalStatus(true)}>
                <Icon type="eye"/> Detail
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="info-circle"/> Detil Lembur
                        </span>
                }
                visible={this.state.modalVisible}
                width={600}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout} className={"form-detail"}>
                    <Form.Item
                        label="Staf">
                        {this.props.record.staff.staff_name}
                    </Form.Item>
                    <Form.Item
                        label="Posisi">
                        {this.props.record.staff.position.structure_name}
                    </Form.Item>
                    <Form.Item
                        label="Waktu Pengajuan Lembur">
                        {this.props.record.overtime_date_request_format}
                    </Form.Item>
                    <Form.Item
                        label="Tanggal Lembur">
                        {this.props.record.overtime_date_format}
                    </Form.Item>
                    <Form.Item
                        label="Keterangan">
                        {this.props.record.overtime_reason}
                    </Form.Item>
                    <br/>
                    <div className={"text-center"}>
                        {/*<Button type={"success"}><Icon type={"check"} /> Setujui</Button>*/}
                        {/*&nbsp;*/}
                        {/*<Button type={"danger"}><Icon type={"close"} /> Tolak</Button>*/}
                        {/*&nbsp;*/}
                        <Button onClick={this.modalStatus(false)}><Icon type={"rollback"}/> Kembali</Button>
                    </div>
                </Form>

            </Modal>
        ]
    }

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 10},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 14},
    },
};