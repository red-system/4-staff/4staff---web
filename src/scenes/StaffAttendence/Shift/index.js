import React, {Component} from 'react';
import {Tabs, Icon, Row, Col} from "antd";

import ShiftCalendarView from './1_ShiftCalendarView';
import ShiftManage from './2_ShiftManage';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {menuActionViewAccess} from "../../../services/app/General";

const {TabPane} = Tabs;


export default class RewardGuide extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true
        }
    }

    componentWillMount() {
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })
    }

    render() {

        let menuCalendar = menuActionViewAccess('staff_attendence', 'shift', 'calendar_shift');

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>

                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="1">
                                <TabPane
                                    tab={
                                        <span>
                                          <Icon type="pic-center"/>
                                          Jadwal Shift
                                        </span>
                                    }
                                    key="1"
                                    className={menuCalendar.class}>
                                    <ShiftCalendarView
                                        listRefresh={this.props.listRefresh}
                                        listRefreshRun={this.props.listRefreshRun}/>
                                </TabPane>
                                <TabPane
                                    tab={
                                        <span>
                              <Icon type="ordered-list"/>
                              Pengaturan Shift
                            </span>
                                    }
                                    key="2">
                                    <ShiftManage
                                        listRefresh={this.props.listRefresh}
                                        listRefreshRun={this.props.listRefreshRun}/>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

let pageNow = GENERALDATA.primaryMenu.staff_attendence.sub_menu["6"];
let pageParent = GENERALDATA.primaryMenu.staff_attendence;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];