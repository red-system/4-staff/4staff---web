import React, { Component } from "react";
import { Button, Table } from "antd";

import Edit from "../2_ShiftManageEdit";
import Delete from "../2_ShiftManageDelete";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";

class ShiftManageView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    render() {
        let menuList = menuActionViewAccess("staff_attendence", "shift", "list_shift");
        let menuEdit = menuActionViewAccess("staff_attendence", "shift", "edit_shift");
        let menuDelete = menuActionViewAccess("staff_attendence", "shift", "delete_shift");

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Nama Shift",
                dataIndex: "shiftm_name",
                key: "shiftm_name",
            },
            {
                title: "Jam Hadir",
                dataIndex: "shiftm_present_hour",
                key: "shiftm_present_hour",
            },
            {
                title: "Jam Pulang",
                dataIndex: "shiftm_return_hour",
                key: "shiftm_return_hour",
            },
            {
                title: "Toleransi",
                dataIndex: "shiftm_tolerance_minutes",
                key: "shiftm_tolerance_minutes",
                render: (text, record) => {
                    return record.shiftm_tolerance_minutes + " menit";
                },
            },
            {
                title: "Keterangan",
                dataIndex: "shiftm_description",
                key: "shiftm_description",
            },
            {
                title: "Total Jam Kerja",
                dataIndex: "shiftm_total_work_hour",
                key: "shiftm_total_work_hour",
                render: (text, record) => {
                    return record.shiftm_total_work_hour + " jam";
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.data.map((item, key) => {
            return {
                key: item["shift_master_id"],
                no: key + 1,
                ...item,
            };
        });

        return <Table className={menuList.class} columns={columns} loading={this.state.isLoading} dataSource={data} bordered={true} size="small" />;
    }

    list() {
        this.setState({
            isLoading: true,
        });

        GetData("/shift-master").then((res) => {
            const data = res.data.data;
            if (res.data.status === "success") {
                this.setState({
                    isLoading: false,
                    data: data,
                });
            }
            // this.setState({ data: data });
        });
    }
}

export default ShiftManageView;
