import React, {Component} from 'react';
import {Button, Form, Icon, Modal, DatePicker, Input, message} from "antd";
import moment from 'moment';
import {PostData, PutData} from "../../../../services/api";
import {dateFormatApi, dateFormatApp, dateFormatterApp, dateToDay} from "../../../../services/app/General";

class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formError: [],
            isLoading: false,
            formValue: props.record
        };
    }

    render() {

        return [
            <Button type="success" onClick={this.modalStatus(true)} icon={"edit"}>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="plus"/> Data Shift Baru
                        </span>
                }
                visible={this.state.modalVisible}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Judul Dayoff"
                        {...this.state.formError.dayoff_title}>
                        <Input defaultValue={this.state.formValue.dayoff_title}
                               onChange={this.onChange("dayoff_title")}/>
                    </Form.Item>
                    <Form.Item
                        label="Keterangan"
                        {...this.state.formError.dayoff_description}>
                        <Input.TextArea defaultValue={this.state.formValue.dayoff_description}
                                        onChange={this.onChange("dayoff_description")}/>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType="submit" icon={"check"}
                                loading={this.state.isLoading} onClick={this.insert}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }

    handleDatePickerChange = (date, dateString, id) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                dayoff_date: dateString
            }
        });
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };

    onChangeDate = name => value => {
        value = value != null ? value.format(dateFormatApi()) : '';

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };


    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };


    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData(`/day-off/${this.props.record.dayoff_id}`, this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                        formError: [],
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 6,
        },
    },
};

export default Create;