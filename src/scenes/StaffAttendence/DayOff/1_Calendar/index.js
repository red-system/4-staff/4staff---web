import React, { Component } from "react";
import { Calendar, Badge, Spin, Alert, Modal, Row, Col, Button } from "antd";
import { GetData } from "../../../../services/api";
import list from "less/lib/less/functions/list";
import { dateFormatterApp, menuActionViewAccess } from "../../../../services/app/General";
import DayOffCreate from "../2_DayOff_Create";
import DayOffList from "../2_DayOff_List";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            modalVisible: false,
            dateSelected: "",
        };
    }

    componentWillMount() {
        this.data();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.data();
        }
    }

    render() {
        let menuPreview = menuActionViewAccess("my_company", "dayoff", "preview");

        const { data } = this.state;

        function getListData(value) {
            let listData = [];
            let year = value.format("YYYY");
            let month = value.format("MM");
            let day = value.format("DD");
            let date = `${year}-${month}-${day}`;

            Object.keys(data).map((key) => {
                if (key == date) {
                    data[key].map((item, key2) => {
                        listData[key2] = {
                            type: "success",
                            content: `${item["dayoff_title"]} - ${item["dayoff_description"]}`,
                        };
                    });
                }
            });

            return listData || [];
        }

        function dateCellRender(value) {
            const listData = getListData(value);
            return (
                <ul className="events">
                    {listData.map((item) => (
                        <li key={item.content}>
                            <Badge status={item.type} text={item.content} />
                        </li>
                    ))}
                </ul>
            );
        }

        function getMonthData(value) {
            if (value.month() === 8) {
                return 1394;
            }
        }

        function monthCellRender(value) {
            const num = getMonthData(value);
            return num ? (
                <div className="notes-month">
                    <section>{num}</section>
                    <span>Backlog number</span>
                </div>
            ) : null;
        }

        return (
            <Spin tip={"Loading ..."} spinning={this.state.isLoading} className={menuPreview.class}>
                <Alert message="Untuk menambah dayoff/hari libur, silahkan klik tanggal selanjutnya bisa manage datanya" banner closable />
                <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} onSelect={this.onSelect} />

                <Modal
                    title={`Data Libur Tanggal : ${dateFormatterApp(this.state.dateSelected)}`}
                    visible={this.state.modalVisible}
                    onCancel={this.modalStatus(false)}
                    width={900}
                    footer={[
                        <Row>
                            <Col>
                                <Button key="back" onClick={this.modalStatus(false)}>
                                    Tutup
                                </Button>
                            </Col>
                        </Row>,
                    ]}
                >
                    <DayOffCreate date={this.state.dateSelected} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                    <DayOffList date={this.state.dateSelected} listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                </Modal>
            </Spin>
        );
    }

    data() {
        this.setState({
            isLoading: true,
        });
        GetData("/day-off-calendar").then((res) => {
            const data = res.data.data;
            this.setState({
                data: data,
                isLoading: false,
            });
        });
    }

    onSelect = (value) => {
        this.setState({
            modalVisible: true,
            dateSelected: value.format("YYYY-MM-DD"),
        });
        this.props.listRefreshRun(true);
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
    };
}

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 18,
        offset: 6,
    },
    lg: {
        span: 18,
        offset: 6,
    },
};
