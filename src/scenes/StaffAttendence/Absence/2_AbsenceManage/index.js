import React, {Component} from 'react';

import Create from "../2_AbsenceManageCreate";
import Import from "../2_AbsenceManageImport";
import Data from "../2_AbsenceManageData";
import {menuActionViewAccess} from "../../../../services/app/General";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        let menuCreate = menuActionViewAccess('staff_attendence', 'absence', 'create_manage');
        let menuImport = menuActionViewAccess('staff_attendence', 'absence', 'import_manage');

        return (
            <span>
                <span className={menuCreate.class}>
                    <Create
                        listRefresh={this.props.listRefresh}
                        listRefreshRun={this.props.listRefreshRun}
                        dataStaff={this.props.dataStaff}
                        isLoadingStaff={this.props.isLoadingStaff}
                        absenceOnBreak={this.props.absenceOnBreak}/>
                </span>
                &nbsp;
                <span className={menuImport.class}>
                    <Import
                        listRefresh={this.props.listRefresh}
                        listRefreshRun={this.props.listRefreshRun}
                        dataStaff={this.props.dataStaff}
                        isLoadingStaff={this.props.isLoadingStaff}
                        absenceOnBreak={this.props.absenceOnBreak}/>
                </span>

                    <br/>
                    <br/>

                <Data
                    listRefresh={this.props.listRefresh}
                    listRefreshRun={this.props.listRefreshRun}
                    dataStaff={this.props.dataStaff}
                    isLoadingStaff={this.props.isLoadingStaff}
                    absenceOnBreak={this.props.absenceOnBreak}/>
            </span>
        )
    }
}