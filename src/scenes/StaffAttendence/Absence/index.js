import React, { Component } from "react";
import { Col, Icon, Row, Tabs } from "antd";

import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from "../../../components/Breadcrumb";

import AbsenceSummary from "./1_AbsenceSummaryList";
import AbsenceManage from "./2_AbsenceManage";
import AbsencePublish from "./3_AbsencePublish";

import { GetData } from "../../../services/api";

export default class RewardTabs extends Component {
    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            listRefresh: true,
            dataStaff: [],
            isLoadingStaff: true,
            absenceOnBreak: "no",
        };
    }

    componentWillMount() {
        this.dataStaff();
        this.absence_on_break_status();
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    render() {
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="absence-summary">
                                <TabPane
                                    tab={
                                        <span>
                                            <Icon type="border-outer" /> Rangkuman Absen
                                        </span>
                                    }
                                    key="absence-summary"
                                >
                                    <AbsenceSummary
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}
                                        dataStaff={this.state.dataStaff}
                                        isLoadingStaff={this.state.isLoadingStaff}
                                    />
                                </TabPane>
                                <TabPane
                                    tab={
                                        <span>
                                            <Icon type="ordered-list" /> Manajemen Absen
                                        </span>
                                    }
                                    key="absence-management"
                                >
                                    <AbsenceManage
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}
                                        dataStaff={this.state.dataStaff}
                                        isLoadingStaff={this.state.isLoadingStaff}
                                        absenceOnBreak={this.state.absenceOnBreak}
                                    />
                                </TabPane>
                                <TabPane
                                    tab={
                                        <span>
                                            <Icon type="check" /> Publish Absen
                                        </span>
                                    }
                                    key="absence-publish"
                                >
                                    <AbsencePublish
                                        listRefresh={this.state.listRefresh}
                                        listRefreshRun={this.listRefreshRun}
                                        absenceOnBreak={this.state.absenceOnBreak}
                                    />
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value,
        });
    };

    dataStaff = () => {
        this.setState({
            isLoadingStaff: true,
        });
        GetData("/staff").then((result) => {
            this.setState({
                dataStaff: result.data.data,
                isLoadingStaff: false,
            });
        });
    };

    absence_on_break_status() {
        this.setState({
            isLoading: true,
        });
        GetData("/absence-on-break-status").then((result) => {
            this.setState({
                absenceOnBreak: result.data.data.status,
            });
        });
    }
}

let pageNow = GENERALDATA.primaryMenu.staff_attendence.sub_menu["3"];
let pageParent = GENERALDATA.primaryMenu.staff_attendence;
const { TabPane } = Tabs;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];
