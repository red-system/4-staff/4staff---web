import React, {Component} from 'react';
import {Button, Icon, Modal} from "antd";
import AbsenceSymmaryDetailTable from "../1_AbsenceSummaryDetailTable";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        };
    }

    render() {

        //console.log(this.props.record);

        return (
            <div key={this.props.record.staff_id}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="poweroff"/>
                    Detil Absen
                </Button>
                <Modal
                    title={<span><Icon type={"user"}/> Detil Absen - {this.props.record.staff_name}</span>}
                    visible={this.state.modalVisible}
                    width={"90%"}
                    onCancel={this.modalStatus(false)}
                    footer={[
                        <div className={"text-center"}>
                            <Button key="back" type={"warning"} onClick={this.modalStatus(false)} icon={"close"}>
                                Tutup
                            </Button>
                        </div>
                    ]}
                >
                    <AbsenceSymmaryDetailTable staff_id={this.props.record.staff_id}/>
                </Modal>
            </div>
        );
    }


    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });
    };
}