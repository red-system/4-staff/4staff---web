import React, {Component} from 'react';
import {Button, Icon, Modal, TimePicker, message, Alert} from "antd";
import moment from 'moment';
import {PostData} from "../../../../services/api";
import renderHtml from "react-render-html";
import {dayId} from "../../../../services/app/General";


message.config({
    top: 24,
    duration: 10
});

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: props.modalVisibleErrorTime,
            errorTimeData: props.errorTimeData,
            isLoading: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            modalVisible: nextProps.modalVisibleErrorTime,
            errorTimeData: nextProps.errorTimeData,
        });
    }

    render() {

        let {errorTimeData} = this.state;
        let absence_year = this.state.errorTimeData.absence_year;
        let absence_month = this.state.errorTimeData.absence_month;
        let freedays = errorTimeData.freedays;
        let freedaysClass = '';

        return (
            <span>
                <Modal
                    title={
                        <span>
                            <Icon
                                type="import"/> Perbaikan Waktu Absensi
                        </span>
                    }
                    visible={this.state.modalVisible}
                    onCancel={this.modalStatus(false)}
                    width={"98%"}
                    style={{top: 20}}
                    footer={null}
                >
                    <h4 align={"center"}>Tahun : {absence_year}</h4>
                    <h4 align={"center"}>Bulan : {absence_month}</h4>
                    <Alert message="Kolom Absen Tanggal warna merah merupakan, hari libur kantor" type="info" />
                    <br />
                    <br />

                    <div style={{overflow: "auto"}}>
                        <table className="table-bordered">
                            <thead className={"ant-table-thead"}>
                                  <tr>
                                    <th className={"text-center"} rowSpan="3">No</th>
                                    <th className={"text-center"} rowSpan="3"
                                        style={{width: "250px !important"}}>ID Staff</th>
                                    <th className={"text-center"} rowSpan="3"
                                        style={{width: "250px !important"}}>
                                        Menu
                                        <br />
                                        <br />
                                        <Button type="primary" icon={"clock-circle"} onClick={this.createOffceTimeAll()} size={"small"}>Isikan semua jam kosong</Button>
                                    </th>
                                    <th className={"text-center"} colSpan={parseInt(errorTimeData.days_count) * 2}>Tanggal</th>
                                  </tr>
                                  <tr>
                                      {
                                          Object.keys(errorTimeData.days).map((key) => {
                                              let day = errorTimeData.days[key];
                                              let check = moment(`${absence_year}/${absence_month}/${day}`, 'YYYY/MM/DD');
                                              let days = `${check.format('DD/MM/YYYY')} - ${dayId(check.format('dddd'))}`;
                                              let freedaysClass = freedays.includes(check.format('dddd')) ? 'freedays':'';
                                              return <th className={freedaysClass} colSpan="2">{days}</th>
                                          })
                                      }
                                  </tr>
                                  <tr>
                                      {
                                          Object.keys(errorTimeData.days).map((key) => {
                                              let day = errorTimeData.days[key];
                                              let check = moment(`${absence_year}/${absence_month}/${day}`, 'YYYY/MM/DD');
                                              let freedaysClass = freedays.includes(check.format('dddd')) ? 'freedays':'';
                                              return [
                                                  <th className={"text-center "+freedaysClass}>Hadir</th>,
                                                  <th className={"text-center "+freedaysClass}>Pulang</th>
                                              ]
                                          })
                                      }
                                  </tr>
                            </thead>
                            <tbody className={"ant-table-tbody"}>
                            {
                                Object.keys(errorTimeData.data).map((key) => {
                                    let no = parseInt(key) + 1;
                                    let staff = `<strong>${errorTimeData.data[key]['staff_id_label']}</strong> <br /> ${errorTimeData.data[key]['staff_name']}`;
                                    return (
                                        <tr>
                                            <td>{no}.</td>
                                            <td style={{width: "250px !important"}}>
                                                {renderHtml(staff)}
                                            </td>
                                            <td>
                                                <Button type="primary" icon={"clock-circle"} onClick={this.createOfficeTime(key)} size={"small"}>Isikan jam kosong</Button>
                                            </td>
                                            {
                                                Object.keys(errorTimeData.days).map((key2) => {
                                                    let day = errorTimeData.days[key2];
                                                    let date = absence_year + "-" + absence_month + '-' + day;
                                                    let present_hour = '';
                                                    let return_hour = '';

                                                    let check = moment(`${absence_year}/${absence_month}/${day}`, 'YYYY/MM/DD');
                                                    let freedaysClass = freedays.includes(check.format('dddd')) ? 'freedays':'';


                                                    if (date in errorTimeData.data[key]['absence']) {
                                                        if(errorTimeData.data[key]['absence'][date]['present_hour'] != null) {
                                                            present_hour = moment(errorTimeData.data[key]['absence'][date]['present_hour'] + ":00", 'HH:mm:ss');
                                                        }
                                                        if(errorTimeData.data[key]['absence'][date]['return_hour'] != null) {
                                                            return_hour = moment(errorTimeData.data[key]['absence'][date]['return_hour'] + ":00", 'HH:mm:ss');
                                                        }
                                                    }

                                                    return [
                                                        <th className={freedaysClass}>
                                                            <TimePicker
                                                                value={present_hour}
                                                                size="small"
                                                                style={{width: "100px"}}
                                                                onChange={this.onChangeTime(key, date, 'present_hour')}
                                                                placeholder={""}/>
                                                        </th>,
                                                        <th className={freedaysClass}>
                                                            <TimePicker
                                                                value={return_hour}
                                                                size="small"
                                                                style={{width: "100px"}}
                                                                onChange={this.onChangeTime(key, date, 'return_hour')}
                                                                placeholder={""}/>
                                                        </th>
                                                    ]
                                                })
                                            }
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>

                    <div className={"wrapper-button-float"}>
                        <Button type="primary" size={"large"} icon={"check"} htmlType="submit" loading={this.state.isLoading} onClick={this.insert}>
                            Simpan Data Absen
                        </Button>
                        &nbsp;
                        <Button type="warning" size={"large"} icon={"rollback"} htmlType="submit" loading={this.state.isLoading} onClick={this.modalStatus(false)}>
                            Kembali
                        </Button>
                    </div>
                </Modal>
            </span>
        )
    }

    createOffceTimeAll = () => e => {
        let {errorTimeData} = this.state;
        let data = errorTimeData.data;
        let workdays = errorTimeData.workdays;

        Object.keys(data).map((key_row) => {
            Object.keys(data[key_row]['absence']).map((date) => {
                let present_hour = data[key_row]['absence'][date]['present_hour'];
                let return_hour = data[key_row]['absence'][date]['return_hour'];
                let date_moment = moment(`${date}`, 'YYYY/MM/DD');
                let day = date_moment.format('dddd');

                let workday_present_hour = workdays[day][0]['present_hour'];
                let workday_return_hour = workdays[day][0]['return_hour'];

                if(present_hour == null) {
                    data[key_row]['absence'][date]['present_hour'] = workday_present_hour;
                }

                if(return_hour == null) {
                    data[key_row]['absence'][date]['return_hour'] = workday_return_hour;
                }

                data[key_row]['absence'][date]['workdays_id'] = workdays[day][0]['workdays_id'];

            });
        });

        this.setState({
            errorTimeData: {
                ...this.state.errorTimeData,
                data: data
            }
        });
    };

    createOfficeTime = (key_row) => e => {
        let {errorTimeData} = this.state;
        let data = errorTimeData.data;
        let workdays = errorTimeData.workdays;

        Object.keys(data[key_row]['absence']).map((date) => {
            let present_hour = data[key_row]['absence'][date]['present_hour'];
            let return_hour = data[key_row]['absence'][date]['return_hour'];
            let date_moment = moment(`${date}`, 'YYYY/MM/DD');
            let day = date_moment.format('dddd');

            let workday_present_hour = workdays[day][0]['present_hour'];
            let workday_return_hour = workdays[day][0]['return_hour'];

            if(present_hour == null) {
                data[key_row]['absence'][date]['present_hour'] = workday_present_hour;
            }

            if(return_hour == null) {
                data[key_row]['absence'][date]['return_hour'] = workday_return_hour;
            }

            data[key_row]['absence'][date]['workdays_id'] = workdays[day][0]['workdays_id'];

        });

        this.setState({
            errorTimeData: {
                ...this.state.errorTimeData,
                data: data
            }
        });

    };

    onChangeTime = (key, date, name) => time => {

        let {errorTimeData} = this.state;
        let hour = time.format('HH:mm:ss');
        if (date in errorTimeData.data[key]['absence']) {
            errorTimeData.data[key]['absence'][date][name] = hour;
        } else {
            errorTimeData.data[key]['absence'] = {
                [date]: date
            };

            errorTimeData.data[key]['absence'] = {
                [name]: time.format('HH:mm:ss')
            }
        }

        this.setState({
            errorTimeData: errorTimeData
        });
    };

    modalStatus = (value) => e => {
        this.setState({
            modalVisible: value,
        });

        if (!value) {
            this.setState({
                formError: []
            })
        }
    };


    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        let data = {
            absence_data: this.state.errorTimeData.data
        };

        PostData('/absence-import-insert', data)
            .then((result) => {
                const data = result.data;

                if (data.status === 'success') {
                    message.destroy();
                    message.success(data.message);
                    this.props.listRefreshRun(true);
                    this.props.modalImportClose();
                    this.props.modalStatusErrorTimeClose();
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];
                    message.warning(data.message);

                    Object.keys(errors).map(function (key) {
                        formError[key] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });
        return false;
    }
}