import React, {Component} from "react";
import {PostData} from "../../../../services/api";
import {menuActionViewAccess, yearToDay} from "../../../../services/app/General";
import {Form, Select} from "antd";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            absence_year: yearToDay(),
            staff_id: props.staff_id
        }
    }

    componentWillMount() {
        this.summary(this.state.absence_year);
    }

    onChange = (value) => {
        this.summary(value);
        this.setState({
            absence_year: value
        })
    };

    render() {
        let menuList = menuActionViewAccess('personal_menu', 'absence', 'list');
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 21},
            },
        };

        let select_option = [];

        for (let i = 2015; i < 2030; i++) {
            select_option.push(
                <Select.Option value={i}>{i}</Select.Option>
            )
        }

        return (
            <div>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item label="Filter Tahun Absen">
                        <Select
                            showSearch
                            style={{width: 200}}
                            placeholder="Pilih Tahun"
                            optionFilterProp="children"
                            onChange={this.onChange}
                            defaultValue={this.state.absence_year}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {
                                select_option
                            }
                        </Select>
                    </Form.Item>
                </Form>


                <table className={"table-bordered " + menuList.class}>
                    <tbody>
                    <tr>
                        <td><strong>Deskripsi</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td className="text-center">
                                    <strong>{`${item['monthIndo']}`}</strong></td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Waktu Telat (menit)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_late'] == null ? "-" : item['total_late']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Kelebihan Istirahat (menit)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_overbreak'] == null ? "-" : item['total_overbreak']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Ijin Keluar (menit)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_skipping'] == null ? "-" : item['total_skipping']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Lembur (menit)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_overtime'] == null ? "-" : item['total_overtime']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Cuti (hari)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_leave'] == null ? "-" : item['total_leave']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Cuti Terbayar (hari)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_paid_leave'] == null ? "-" : item['total_paid_leave']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Cuti Tidak Terbayar (hari)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_unpaid_leave'] == null ? "-" : item['total_unpaid_leave']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Hari Kerja Kantor (hari)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_working_days'] == null ? "-" : item['total_working_days']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Hari Staf Kerja (hari)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_work'] == null ? "-" : item['total_work']}</td>
                            })
                        }
                    </tr>
                    <tr>
                        <td><strong>Total Hari Staf Tidak Bekerja (hari)</strong></td>
                        {
                            this.state.data.map((item, key) => {
                                return <td
                                    className="text-center">{item['total_not_work'] == null ? "-" : item['total_not_work']}</td>
                            })
                        }
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }

    summary = (absence_year) => {
        let data = {
            absence_year: absence_year,
            staff_id: this.state.staff_id
        };
        PostData(`/absence-summary`, data)
            .then((result) => {
                this.setState({
                    data: result.data.data
                });
            });
    };

}