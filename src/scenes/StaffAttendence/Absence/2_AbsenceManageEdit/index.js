import React, { Component } from "react";
import { Button, Form, Icon, Modal, DatePicker, TimePicker, Select, message, Radio } from "antd";
import moment from "moment";
import { PostData, PutData } from "../../../../services/api";
import { dateFormatApi, dateFormatApp, dateFormatterApp } from "../../../../services/app/General";

export default class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: props.record,
        };
    }

    render() {
        return [
            <Button type="success" onClick={this.modalStatus(true)} icon={"edit"} key={this.props.key}>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Absen
                    </span>
                }
                visible={this.state.modalVisible}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Nama Staff" {...this.state.formError.staff_id}>
                        <Select
                            showSearch
                            placeholder="Pilih Staff"
                            optionFilterProp="children"
                            value={this.state.formValue.staff_id}
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            onChange={this.onChange("staff_id")}
                        >
                            {this.props.dataStaff.map((item, key) => {
                                return (
                                    <Select.Option key={key} value={item["staff_id"]}>
                                        {item["staff_name"]}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item label="Tanggal" {...this.state.formError.absence_date}>
                        <DatePicker
                            style={{ width: "100%" }}
                            onChange={this.onChangeDate("absence_date")}
                            defaultValue={moment(dateFormatterApp(this.state.formValue.absence_date), dateFormatApp())}
                            format={dateFormatApp()}
                        />
                    </Form.Item>
                    <Form.Item label="Jam Hadir" {...this.state.formError.present_hour}>
                        <TimePicker
                            style={{ width: "100%" }}
                            defaultValue={moment(this.state.formValue.present_hour ?? "00:00:00", "HH:mm:ss")}
                            onChange={this.onChangeTime("present_hour")}
                        />
                    </Form.Item>
                    {this.props.absenceOnBreak === "yes"
                        ? [
                              <Form.Item label="Mulai Istirahat" {...this.state.formError.break_in}>
                                  <TimePicker
                                      style={{ width: "100%" }}
                                      defaultValue={moment(this.state.formValue.break_in ?? "00:00:00", "HH:mm:ss")}
                                      onChange={this.onChangeTime("break_in")}
                                  />
                              </Form.Item>,
                              <Form.Item label="Selesai Istirahat" {...this.state.formError.break_out}>
                                  <TimePicker
                                      style={{ width: "100%" }}
                                      defaultValue={moment(this.state.formValue.break_out ?? "00:00:00", "HH:mm:ss")}
                                      onChange={this.onChangeTime("break_out")}
                                  />
                              </Form.Item>,
                          ]
                        : ""}

                    <Form.Item label="Jam Pulang" {...this.state.formError.return_hour}>
                        <TimePicker
                            style={{ width: "100%" }}
                            defaultValue={moment(this.state.formValue.return_hour ?? "00:00:00", "HH:mm:ss")}
                            onChange={this.onChangeTime("return_hour")}
                        />
                    </Form.Item>

                    {/*<Form.Item*/}
                    {/*    label="Status Lembur"*/}
                    {/*    {...this.state.formError.overtime_status}>*/}
                    {/*    <Radio.Group*/}
                    {/*        buttonStyle="solid"*/}
                    {/*        defaultValue={this.state.formValue.overtime_status}*/}
                    {/*        onChange={this.onChange('overtime_status')}>*/}
                    {/*        <Radio.Button value="yes">Lembur</Radio.Button>*/}
                    {/*        <Radio.Button value="no">Tidak Lembur</Radio.Button>*/}
                    {/*    </Radio.Group>*/}
                    {/*</Form.Item>*/}
                    {/*{*/}
                    {/*    this.state.formValue.overtime_status === "yes" ? [*/}
                    {/*        <Form.Item*/}
                    {/*            label="Mulai Lembur"*/}
                    {/*            {...this.state.formError.overtime_start}>*/}
                    {/*            <TimePicker*/}
                    {/*                style={{width: "100%"}}*/}
                    {/*                defaultValue={moment(this.state.formValue.overtime_start, 'HH:mm:ss')}*/}
                    {/*                onChange={this.onChangeTime("overtime_start")}/>*/}
                    {/*        </Form.Item>,*/}
                    {/*        <Form.Item*/}
                    {/*            label="Selesai Lembur"*/}
                    {/*            {...this.state.formError.overtime_end}>*/}
                    {/*            <TimePicker*/}
                    {/*                style={{width: "100%"}}*/}
                    {/*                defaultValue={moment(this.state.formValue.overtime_end, 'HH:mm:ss')}*/}
                    {/*                onChange={this.onChangeTime("overtime_end")}/>*/}
                    {/*        </Form.Item>*/}
                    {/*    ] : ""*/}
                    {/*}*/}

                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.update}>
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    onChangeDate = (name) => (value) => {
        value = value != null ? value.format(dateFormatApi()) : "";

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    handleDatePickerChange = (date, dateString, id) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                absence_date: dateString,
            },
        });
    };

    onChangeTime = (name) => (time) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: time.format("HH:mm:ss"),
            },
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        console.log(this.state.formValue);
    };

    update = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        PostData(`/absence/${this.state.formValue.absence_id}`, this.state.formValue).then((result) => {
            const data = result.data;

            if (data.status === "success") {
                message.success(data.message);
                this.props.listRefreshRun(true);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formError: [],
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[key] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });
        return false;
    };
}

const date_format = "YYYY-MM-DD";

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 17 },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 7,
        },
    },
};
