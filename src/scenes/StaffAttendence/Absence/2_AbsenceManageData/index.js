import React, { Component } from "react";
import { Button, Icon, Table, Tag, Input } from "antd";

import Edit from "../2_AbsenceManageEdit";
import Delete from "../2_AbsenceManageDelete";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess, status_view } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffName = false;
            if (get(record, "staff_name") != null) {
                staffName = get(record, "staff_name").toString().match(reg);
            }
            var asbsenceDate = false;
            if (get(record, "absence_date_format") != null) {
                asbsenceDate = get(record, "absence_date_format").toString().match(reg);
            }
            var presentHour = false;
            if (get(record, "present_hour") != null) {
                presentHour = get(record, "present_hour").toString().match(reg);
            }
            var returnHour = false;
            if (get(record, "return_hour") != null) {
                returnHour = get(record, "return_hour").toString().match(reg);
            }
            if (!staffName && !asbsenceDate && !presentHour && !returnHour) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let status_absence_on_break = this.props.absenceOnBreak === "yes" ? "" : "hide";
        let total_data = 0;
        let total_data_publish_yes = 0;
        let total_data_publish_no = 0;

        let menuList = menuActionViewAccess("staff_attendence", "absence", "list_manage");
        let menuEdit = menuActionViewAccess("staff_attendence", "absence", "edit_manage");
        let menuDelete = menuActionViewAccess("staff_attendence", "absence", "delete_manage");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Tanggal",
                dataIndex: "absence_date_format",
                key: "absence_date_format",
            },
            {
                title: "Jam Hadir",
                dataIndex: "present_hour",
                key: "present_hour",
            },
            // {
            //     title: 'Mulai Istirahat',
            //     dataIndex: 'break_in',
            //     key: 'break_in',
            //     className: status_absence_on_break
            // },
            // {
            //     title: 'Selesai Istirahat',
            //     dataIndex: 'break_out',
            //     key: 'break_out',
            //     className: status_absence_on_break
            // },
            {
                title: "Jam Pulang",
                dataIndex: "return_hour",
                key: "return_hour",
            },
            // {
            //     title: 'Mulai Lembur',
            //     dataIndex: 'overtime_start',
            //     key: 'overtime_start',
            //     render: (text, record) => {
            //         if(record.overtime_status === 'yes') {
            //             return record.overtime_start
            //         }
            //     }
            // },
            // {
            //     title: 'Selesai Lembur',
            //     dataIndex: 'overtime_end',
            //     key: 'overtime_end',
            //     render: (text, record) => {
            //         if (record.overtime_status === 'yes') {
            //             return record.overtime_end
            //         }
            //     }
            // },
            {
                title: "Publish",
                dataIndex: "absence_publish",
                key: "absence_publish",
                render: (text, record) => {
                    return status_view(record.absence_publish);
                },
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 160,
                render: (text, record) => {
                    console.log(record);
                    if (record.absence_publish === "no") {
                        return (
                            <Button.Group size={"small"}>
                                <span className={menuEdit.class}>
                                    <Edit record={record} dataStaff={this.props.dataStaff} listRefreshRun={this.props.listRefreshRun} key={record.key + 1} />
                                </span>
                                <span className={menuDelete.class}>
                                    <Delete record={record} listRefreshRun={this.props.listRefreshRun} />
                                </span>
                            </Button.Group>
                        );
                    }
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            total_data++;
            if (item["absence_publish"] === "yes") {
                total_data_publish_yes++;
            } else {
                total_data_publish_no++;
            }
            return {
                key: item["absence_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuList.class}
                    columns={columns}
                    dataSource={data}
                    bordered={true}
                    title={() => {
                        return (
                            <span>
                                <Tag color={"cyan"}>
                                    <Icon type={"ordered-list"} /> Total Data Absen : {total_data}
                                </Tag>
                                <Tag color={"blue"}>
                                    <Icon type={"check-square"} /> Total Data Absen Publish : {total_data_publish_yes}
                                </Tag>
                                <Tag color={"volcano"}>
                                    <Icon type={"close-square"} /> Total Data Absen Belum Publish : {total_data_publish_no}
                                </Tag>
                            </span>
                        );
                    }}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/absence").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}
