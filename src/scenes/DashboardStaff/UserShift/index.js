import React, {Component} from 'react';
import {Button, Form} from "antd";
import {GetData, PostData} from "../../../services/api";
import {Link} from "react-router-dom";
import GENERALDATA from "../../../constants/generalData";
import {menuActionViewAccess} from "../../../services/app/General";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shift_year: props.shift_year,
            shift_month: props.shift_month,
            shift_data: props.shift_data,
        }
    };

    componentWillReceiveProps(nextProps, nextContext) {

        console.log('will', nextProps);

        this.setState({
            shift_year: nextProps.shift_year,
            shift_month: nextProps.shift_month,
            shift_data: nextProps.shift_data,
        })
    }

    render() {
        let menuKPI = GENERALDATA.primaryMenu.personal_menu.sub_menu[13];
        let menuAction = menuActionViewAccess('dashboard_staff', 'dashboard_staff_view', 'shift_schedule');
        let {shift_year, shift_month, shift_data} = this.state;

        return (
            <Link to={menuKPI.route} className={menuAction.class}>
                <div className="app-content">
                    <div className="app-content-title">
                        Jadwal Shift Staff : <strong>{`${shift_month} ${shift_year}`}</strong>

                        <Button type={"success"} size={"small"} className={"float-right"} icon={"arrow-right"}> Detail Shift</Button>
                    </div>
                    <div className="app-content-body">
                        <table className="table-bordered no-link-style">
                            <thead className={"ant-table-thead"}>
                            <tr>
                                <th className={"text-center"}>Tanggal</th>
                                <th className={"text-center"}>Hari</th>
                                <th className={"text-center"}>Jenis Shift</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                Object.keys(shift_data).map((index, key) => {
                                    let shift_day = shift_data[index].shift;
                                    let shift_day_count = shift_data[index].shift.length;
                                    return (
                                        <tr>
                                            <td className={"text-center"}>{`${index}`}</td>
                                            <td className={"text-center"}>{`${shift_data[index]['day']}`}</td>
                                            <td>
                                                {
                                                    shift_day_count > 0 ? <ul style={{margin: 0, paddingLeft: "15px"}}>
                                                        {
                                                            shift_day.map((item, key) => {
                                                                return <li>{item.shiftm_name}</li>
                                                            })
                                                        }
                                                    </ul> : 'Nihil'
                                                }
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </Link>
        )
    }

}