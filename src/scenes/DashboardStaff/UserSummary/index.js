import React, {Component} from 'react';
import {Col, Icon, Row, Button} from "antd";
import {Link} from 'react-router-dom';
import GENERALDATA from "../../../constants/generalData";

const gutter = GENERALDATA.gutter;

class AnnouncementNews extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userLevel: props.userLevel,
            summary: props.summary
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            summary: nextProps.summary
        })
    }

    render() {

        let menuKPI = GENERALDATA.primaryMenu.personal_menu.sub_menu[2];
        let menuApproval = GENERALDATA.primaryMenu.key_activity.sub_menu[1];
        let menuReward = GENERALDATA.primaryMenu.personal_menu.sub_menu[14];
        let menuPunishment = GENERALDATA.primaryMenu.personal_menu.sub_menu[15];

        return (
            <Row gutter={gutter}>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Link to={menuKPI.route}>
                        <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-success">
                                        <Icon type="line-chart"/>
                                    </span>
                            <div className="box-info">
                                <p className="box-num">{this.state.summary.kpi === undefined ? "0" : this.state.summary.kpi}
                                    <span className="size-h4">%</span></p>
                                <p className="text-muted" style={{marginBottom: '6px'}}>KPI Terakhir</p>
                            </div>
                        </div>
                    </Link>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Link to={menuApproval.route}>
                        <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-info">
                                        <Icon type="team"/>
                                    </span>
                            <div className="box-info">
                                <p className="box-num">{this.state.summary.approval === undefined ? "0" : this.state.summary.approval}
                                    <span className="size-h4">%</span></p>
                                <p className="text-muted">Approval</p>
                            </div>
                        </div>
                    </Link>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Link to={menuReward.route}>
                        <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-warning">
                                        <Icon type="credit-card"/>
                                    </span>
                            <div className="box-info">
                                <p className="box-num">{this.state.summary.reward === undefined ? "0" : this.state.summary.reward}
                                    <span className="size-h4"></span></p>
                                <p className="text-muted">Reward</p>
                            </div>
                        </div>
                    </Link>
                </Col>
                <Col xs={24} sm={24} md={12} lg={12}>
                    <Link to={menuPunishment.route}>
                        <div className="number-card-v2 mb-3">
                                    <span className="icon-btn icon-btn-round icon-btn-lg text-white bg-primary">
                                        <Icon type="shopping-cart"/>
                                    </span>
                            <div className="box-info">
                                <p className="box-num">{this.state.summary.punishment === undefined ? "0" : this.state.summary.punishment}
                                    <span className="size-h4"></span></p>
                                <p className="text-muted">Punishment</p>
                            </div>
                        </div>
                    </Link>
                </Col>
            </Row>
        )
    }

}

export default AnnouncementNews;