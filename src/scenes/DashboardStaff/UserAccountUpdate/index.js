import {Modal, Button, Form, Input, Row, Col} from 'antd';
import React from "react";

const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const formItemLayout_radius = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 19,
        offset: 8
    },
    'lg': {
        span: 19,
        offset: 8
    }
};



export default class AppComponent extends React.Component {
    state = { visible: false };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    render() {
        return (
            <span>
                <Button type="success"
                        icon="key"
                        size="large"
                        onClick={this.showModal}>
                    Perbarui Akun
                </Button>
                <Modal
                    title="Perbarui Akun"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.handleOk}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.handleCancel}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form>
                        <FormItem
                            {...formItemLayout}
                            label="Username"
                        >
                            <Input/>
                        </FormItem>
                        <FormItem
                            {...formItemLayout_radius}
                            label="Password"
                        >
                            <Input.Password/>
                        </FormItem>
                        <FormItem
                            {...formItemLayout_radius}
                            label="Konfirmasi Password"
                        >
                            <Input.Password/>
                        </FormItem>
                    </Form>
                </Modal>
            </span>
        );
    }
}
