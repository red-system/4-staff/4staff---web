import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import {
    Row,
    Col,
    Button,
    Table, Tag,
} from "antd";

import GENERALDATA from '../../../constants/generalData';
import {GetData} from "../../../services/api";
import Create from "./Create";
import Detail from "../../StaffManagement/StaffResign/Detail"
import moment from "moment";
import {menuActionViewAccess, status_view} from "../../../services/app/General";


let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["12"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        let loginData = localStorage.getItem('loginData');
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            isLoading: true,
            process: 1,
            data: []
        }
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
    }

    render() {

        let menuList = menuActionViewAccess('personal_menu', 'resign', 'list');
        let menuCreate = menuActionViewAccess('personal_menu', 'resign', 'create');
        let menuDetail = menuActionViewAccess('personal_menu', 'resign', 'detail');

        const sample = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        function getSample(value) {
            return sample[value]
        }

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Alasa Resign',
                dataIndex: 'resign_reason',
                key: 'resign_reason'
            },
            {
                title: 'Tanggal Pengajuan',
                dataIndex: 'resign_process_date',
                key: 'resign_process_date',
                render: date => {
                    return moment(date).format("YYYY-MM-DD")
                }
            },
            {
                title: 'Tanggal Resign',
                dataIndex: 'resign_date',
                key: 'resign_date',
                render: date => {
                    return moment(date).format("YYYY-MM-DD")
                }
            },
            {
                title: 'Status',
                dataIndex: 'resign_status',
                key: 'resign_status',
                render: status => {
                    return status_view(status);
                }
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 100,
                render: (test, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDetail.class}>
                            <Detail profile={true} record={record}/>
                        </span>
                    </Button.Group>

                )
            }
        ];


        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['staff_job_history_id'],
                no: key + 1,
                ...item
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <span className={menuCreate.class}>
                                        <Create process={this.state.process} staff_id={this.state.staff_id}
                                                list={this.list}
                                                formLayout={formLayout}/>
                                    </span>
                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                        bordered={true}
                                        dataSource={dataList}
                                        size='small'/>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list = () => {
        GetData(`/staff-resign/${this.state.staff_id}`).then((result) => {
            const data = result.data.data;
            this.setState({
                data: data.resign_data,
                process: data.process,
                isLoading: false
            })
        });
    }
}

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 19,
        offset: 5
    },
    'lg': {
        span: 19,
        offset: 5
    }
};
