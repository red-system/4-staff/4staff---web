import React, { Component } from "react";
import { Button, Col, DatePicker, Form, Icon, Input, InputNumber, message, Modal, Row } from "antd";
import moment from "moment";
import { PostData } from "../../../../services/api";

const { MonthPicker } = DatePicker;
const { TextArea } = Input;

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue: props.record,
            formError: [],
        };
        this.insert = this.insert.bind(this);
    }

    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    render() {
        const { modalEditVisible, formLayout } = this.state;

        return [
            <Button type="success" onClick={this.modalEditShow}>
                <Icon type="edit" />
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="edit" /> Edit Data
                    </span>
                }
                visible={modalEditVisible}
                width={700}
                onOk={this.modalEditSubmit}
                onCancel={this.modalEditCancel}
                footer={[
                    <Row style={{ textAlign: "left" }}>
                        <Col {...formLayout}>
                            <Button key="submit" type="primary" onClick={this.insert()}>
                                Simpan
                            </Button>
                            <Button key="back" onClick={this.modalEditCancel}>
                                Batal
                            </Button>
                        </Col>
                    </Row>,
                ]}
            >
                <Form {...formItemLayout}>
                    <Form.Item label="Perusahaan" {...this.state.formError.staff_jh_company}>
                        <Input
                            value={this.state.formValue.staff_jh_company == undefined ? "" : this.state.formValue.staff_jh_company}
                            onChange={this.onChange("staff_jh_company")}
                        />
                    </Form.Item>
                    <Form.Item label="Dari Bulan/Tahun" {...this.state.formError.staff_jh_date_from}>
                        <MonthPicker
                            value={this.state.formValue.staff_jh_date_from == undefined ? moment() : moment(this.state.formValue.staff_jh_date_from)}
                            onChange={this.onChangeDate("staff_jh_date_from")}
                        />
                    </Form.Item>
                    <Form.Item label="Sampai Bulan/Tahun" {...this.state.formError.staff_jh_date_to}>
                        <MonthPicker
                            value={this.state.formValue.staff_jh_date_to == undefined ? moment() : moment(this.state.formValue.staff_jh_date_to)}
                            onChange={this.onChangeDate("staff_jh_date_to")}
                        />
                    </Form.Item>
                    <Form.Item label="Alamat" {...this.state.formError.staff_jh_address}>
                        <TextArea
                            value={this.state.formValue.staff_jh_address == undefined ? "" : this.state.formValue.staff_jh_address}
                            onChange={this.onChange("staff_jh_address")}
                        />
                    </Form.Item>
                    <Form.Item label="Telepon Perusahaan" {...this.state.formError.staff_jh_telp}>
                        <Input
                            value={this.state.formValue.staff_jh_telp == undefined ? "" : this.state.formValue.staff_jh_telp}
                            onChange={this.onChange("staff_jh_telp")}
                        />
                    </Form.Item>
                    <Form.Item label="Jenis Usaha" {...this.state.formError.staff_jh_bussines_field}>
                        <Input
                            value={this.state.formValue.staff_jh_bussines_field == undefined ? "" : this.state.formValue.staff_jh_bussines_field}
                            onChange={this.onChange("staff_jh_bussines_field")}
                        />
                    </Form.Item>
                    <Form.Item label="Atasan" {...this.state.formError.staff_jh_boss}>
                        <Input
                            value={this.state.formValue.staff_jh_boss == undefined ? "" : this.state.formValue.staff_jh_boss}
                            onChange={this.onChange("staff_jh_boss")}
                        />
                    </Form.Item>
                    <Form.Item label="Deskripsi Pekerjaan" {...this.state.formError.staff_jh_job_description}>
                        <TextArea
                            value={this.state.formValue.staff_jh_job_description == undefined ? "" : this.state.formValue.staff_jh_job_description}
                            onChange={this.onChange("staff_jh_job_description")}
                        />
                    </Form.Item>
                    <Form.Item label="Jabatan" {...this.state.formError.staff_jh_position}>
                        <Input
                            value={this.state.formValue.staff_jh_position == undefined ? "" : this.state.formValue.staff_jh_position}
                            onChange={this.onChange("staff_jh_position")}
                        />
                    </Form.Item>
                    <Form.Item label="Gaji" {...this.state.formError.staff_jh_salary}>
                        <InputNumber
                            style={{ width: "100%" }}
                            defaultValue={1000}
                            value={this.state.formValue.staff_jh_salary == undefined ? "" : this.state.formValue.staff_jh_salary}
                            formatter={(value) => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            parser={(value) => value.replace(/\Rp\s?|(,*)/g, "")}
                            onChange={this.onChange("staff_jh_salary")}
                        />
                    </Form.Item>
                    <Form.Item label="Alasan Keluar" {...this.state.formError.staff_jh_reason_quit}>
                        <TextArea
                            value={this.state.formValue.staff_jh_reason_quit == undefined ? "" : this.state.formValue.staff_jh_reason_quit}
                            onChange={this.onChange("staff_jh_reason_quit")}
                        />
                    </Form.Item>
                    <Form.Item label="File Surat Keterangan Kerja" {...this.state.formError.staff_jh_employment_certificate}>
                        <img src={this.state.formValue.staff_jh_employment_certificate_url} className={"image-preview"} />
                        <br />
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File</button>
                            <input type="file" name="myfile" onChange={this.onChangeFileImage} />
                        </div>
                    </Form.Item>
                </Form>
            </Modal>,
        ];
    }

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    onChangeFileImage = (e) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_jh_employment_certificate_url: URL.createObjectURL(e.target.files[0]),
                staff_jh_employment_certificate: e.target.files[0],
            },
        });
    };
    onChangeDate = (name) => (value) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: moment(value).format("YYYY-MM-DD"),
            },
        });
    };
    insert = () => (e) => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (key == "staff_jh_date_to") {
                formData.append(key, moment(this.state.formValue[key]).format("YYYY-MM-DD"));
            } else if (key == "staff_jh_date_from") {
                formData.append(key, moment(this.state.formValue[key]).format("YYYY-MM-DD"));
            } else {
                formData.append(key, this.state.formValue[key]);
            }
        });
        PostData(`/staff-pekerjaan/${this.props.record.staff_id}/${this.props.record.staff_job_history_id}`, formData).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalEditVisible: false,
                });
                this.props.list();
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
