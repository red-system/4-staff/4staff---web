import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import {
    Row,
    Col,
    Button,
    Table,
} from "antd";

import GENERALDATA from '../../../constants/generalData';
import {GetData} from "../../../services/api";
import Edit from "./Edit";
import Delete from "./Delete";
import Create from "./Create";
import moment from "moment";
import Preview from "./Preview";
import {menuActionViewAccess} from "../../../services/app/General";


let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["9"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        let loginData = localStorage.getItem('loginData');
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            isLoading: true,
            data: []
        };
        this.list = this.list.bind(this);
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
    }


    render() {

        let menuList = menuActionViewAccess('personal_menu', 'job_history', 'list');
        let menuCreate = menuActionViewAccess('personal_menu', 'job_history', 'create');
        let menuEdit = menuActionViewAccess('personal_menu', 'job_history', 'edit');
        let menuDelete = menuActionViewAccess('personal_menu', 'job_history', 'delete');
        let menuPreview = menuActionViewAccess('personal_menu', 'job_history', 'preview');

        const sample = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        function getSample(value) {
            return sample[value]
        }

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Perusahaan',
                dataIndex: 'staff_jh_company',
                key: 'staff_jh_company'
            },
            {
                title: 'Dari Bulan/Tahun',
                dataIndex: 'staff_jh_date_from',
                key: 'staff_jh_date_from',
                render: date => {
                    return getSample(moment(date).format("M")) + " / " + moment(date).format("YYYY")
                }
            },
            {
                title: 'Sampai Bulan/Tahun',
                dataIndex: 'staff_jh_date_to',
                key: 'staff_jh_date_to',
                render: date => {
                    return getSample(moment(date).format("M")) + " / " + moment(date).format("YYYY")
                }
            },
            {
                title: 'Jenis Usaha',
                dataIndex: 'staff_jh_bussines_field',
                key: 'staff_jh_bussines_field'
            },
            {
                title: 'Deskripsi Pekerjaan',
                dataIndex: 'staff_jh_job_description',
                key: 'staff_jh_job_description'
            },
            {
                title: 'Jabatan',
                dataIndex: 'staff_jh_position',
                key: 'staff_jh_position'
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 250,
                render: (test, record) => (

                    <Button.Group size={"small"}>
                        <span className={menuPreview.class}>
                            <Preview record={record}/>
                        </span>
                        <span className={menuEdit.class}>
                            <Edit
                                list={this.list}
                                record={record}
                                formLayout={formLayout}
                                staff_id={this.state.staff_id}/>
                        </span>
                        <span className={menuDelete.class}>
                            <Delete list={this.list} record={record}/>
                        </span>
                    </Button.Group>

                )
            }
        ];


        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <span className={menuCreate.class}>
                                        <Create
                                            list={this.list}
                                            formLayout={formLayout}
                                            staff_id={this.state.staff_id}/>
                                    </span>
                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                           bordered={true}
                                           dataSource={dataList}
                                           loading={this.state.isLoading}
                                           size='small'/>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list = () => {
        GetData(`/staff-pekerjaan/${this.state.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }

}

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 16,
        offset: 8
    },
    'lg': {
        span: 16,
        offset: 8
    }
};
