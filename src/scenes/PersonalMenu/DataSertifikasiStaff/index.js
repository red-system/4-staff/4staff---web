import React, { Component } from "react";
import Breadcrumb from "../../../components/Breadcrumb";
import { Row, Col, Button, Table, Input, Icon } from "antd";

import GENERALDATA from "../../../constants/generalData";
import { GetData } from "../../../services/api";
import Edit from "./Edit";
import Delete from "./Delete";
import Create from "./Create";
import Preview from "./Preview";
import { menuActionViewAccess } from "../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["10"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route,
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`,
    },
];

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        let loginData = localStorage.getItem("loginData");
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [<Breadcrumb data={breadcrumb} key="0" />],
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffCerficateSkill = false;
            if (get(record, "staff_certificate_skill") != null) {
                staffCerficateSkill = get(record, "staff_certificate_skill").toString().match(reg);
            }
            var certificateDate = false;
            if (get(record, "staff_certificate_date_from_format") != null) {
                certificateDate = get(record, "staff_certificate_date_from_format").toString().match(reg);
            }
            var certificateDateTo = false;
            if (get(record, "staff_certificate_date_to_format") != null) {
                certificateDateTo = get(record, "staff_certificate_date_to_format").toString().match(reg);
            }
            var certificateLevel = false;
            if (get(record, "staff_certificate_level") != null) {
                certificateLevel = get(record, "staff_certificate_level").toString().match(reg);
            }
            var certificateDescription = false;
            if (get(record, "staff_certificate_description") != null) {
                certificateDescription = get(record, "staff_certificate_description").toString().match(reg);
            }
            if (!staffCerficateSkill && !certificateDate && !certificateDateTo && !certificateLevel && !certificateDescription) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("personal_menu", "certification_staff", "list");
        let menuCreate = menuActionViewAccess("personal_menu", "certification_staff", "create");
        let menuEdit = menuActionViewAccess("personal_menu", "certification_staff", "edit");
        let menuDelete = menuActionViewAccess("personal_menu", "certification_staff", "delete");
        let menuPreview = menuActionViewAccess("personal_menu", "certification_staff", "preview");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Keterampilan/Kursus",
                dataIndex: "staff_certificate_skill",
                key: "staff_certificate_skill",
            },
            {
                title: "Tanggal Mulai",
                dataIndex: "staff_certificate_date_from_format",
                key: "staff_certificate_date_from_format",
            },
            {
                title: "Tanggal Selesai",
                dataIndex: "staff_certificate_date_to_format",
                key: "staff_certificate_date_to_format",
            },
            {
                title: "Level",
                dataIndex: "staff_certificate_level",
                key: "staff_certificate_level",
            },
            {
                title: "Keterangan",
                dataIndex: "staff_certificate_description",
                key: "staff_certificate_description",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 250,
                render: (test, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuPreview.class}>
                            <Preview record={record} />
                        </span>
                        <span className={menuEdit.class}>
                            <Edit record={record} list={this.list} formLayout={formLayout} />
                        </span>
                        <span className={menuDelete.class}>
                            <Delete record={record} list={this.list} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_certificate_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <span className={menuCreate.class}>
                                        <Create list={this.list} formLayout={formLayout} staff_id={this.state.staff_id} />
                                    </span>
                                    <div className="row mb-1">
                                        <div className="col-9"></div>
                                        <div className="col-3 pull-right">
                                            <Search
                                                size="default"
                                                ref={(ele) => (this.searchText = ele)}
                                                suffix={suffix}
                                                onChange={this.onSearch}
                                                placeholder="Search Records"
                                                value={this.state.searchText}
                                                onPressEnter={this.onSearch}
                                            />
                                        </div>
                                    </div>
                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                        dataSource={dataList}
                                        bordered={true}
                                        loading={this.state.isLoading}
                                        size="small"
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list = () => {
        this.setState({
            isLoading: true,
        });

        GetData(`/staff-sertifikasi/${this.state.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    };
}

const formLayout = {
    xs: {
        span: 24,
    },
    sm: {
        span: 24,
    },
    md: {
        span: 19,
        offset: 5,
    },
    lg: {
        span: 19,
        offset: 5,
    },
};
