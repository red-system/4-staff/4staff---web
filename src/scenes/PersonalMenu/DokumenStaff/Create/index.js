import React, {Component} from 'react';
import {Button, Form, Icon, Input, Modal, Upload, Row, Col, message} from "antd";
import moment from "moment";
import {PostData} from "../../../../services/api";


class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            formLayout: props.formLayout,
            formValue:[],
            formError:[],
        };

    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {

        const {modalCreateVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };

        return (
            <div>
                <Button type="primary" onClick={this.modalCreateShow}>
                    <Icon type="plus"/> Tambah Data
                </Button>
                <br/><br/>

                <Modal
                    title={
                        <span>
                            <Icon type="plus"/> Tambah Data
                        </span>
                    }
                    visible={modalCreateVisible}
                    width={700}
                    onOk={this.modalCreateSubmit}
                    onCancel={this.modalCreateCancel}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalCreateCancel}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="Judul Dokumen"
                            {...this.state.formError.staff_document_title}

                        >
                            <Input value={this.state.formValue.staff_document_title == undefined ? "" : this.state.formValue.staff_document_title} onChange={this.onChange("staff_document_title")}/>
                        </Form.Item>
                        <Form.Item
                            label="Jenis Dokumen"
                            {...this.state.formError.staff_document_type}
                        >
                            <Input value={this.state.formValue.staff_document_type == undefined ? "" : this.state.formValue.staff_document_type} onChange={this.onChange("staff_document_type")} />
                        </Form.Item>
                        <Form.Item
                            label="File Dokumen"
                            {...this.state.formError.staff_document_filename}
                        >
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange}/>
                            </div>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }
    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_document_filename: event.target.files[0]
            }
        })
    };

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });
    };
    insert = (e) => {
        e.preventDefault();
        this.setState({
            isLoading: true
        });

        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            formData.append(key,this.state.formValue[key])
        })

        PostData(`/staff-document/${this.props.staff_id}`, formData)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.list();
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalCreateVisible: false,
                        formValue: []
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }
}

export default Create;