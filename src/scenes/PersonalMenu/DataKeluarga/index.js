import React, {Component} from 'react';
import Breadcrumb from "../../../components/Breadcrumb";
import {
    Row,
    Col,
    Button,
    Table,
} from "antd";

import GENERALDATA from '../../../constants/generalData';
import {GetData} from "../../../services/api";
import Edit from "./Edit";
import Delete from "./Delete";
import Create from "./Create";
import {menuActionViewAccess} from "../../../services/app/General";


let pageNow = GENERALDATA.primaryMenu.personal_menu.sub_menu["7"];
let pageParent = GENERALDATA.primaryMenu.personal_menu;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        let loginData = localStorage.getItem('loginData');
        let staff = JSON.parse(loginData);
        this.state = {
            breadcrumbComponent: null,
            staff_id: staff.staff_id,
            isLoading: true,
            data: []
        }

    }

    componentWillMount() {
        this.list();
        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });
    }

    render() {
        let menuList = menuActionViewAccess('personal_menu', 'family_data', 'list');
        let menuCreate = menuActionViewAccess('personal_menu', 'family_data', 'create');
        let menuEdit = menuActionViewAccess('personal_menu', 'family_data', 'edit');
        let menuDelete = menuActionViewAccess('personal_menu', 'family_data', 'delete');

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Nama',
                dataIndex: 'staff_family_name',
                key: 'staff_family_name'
            },
            {
                title: 'Hubungan',
                dataIndex: 'staff_family_relation',
                key: 'staff_family_relation',
            },
            {
                title: 'Tanggal Lahir',
                dataIndex: 'staff_family_birthday_format',
                key: 'staff_family_birthday_format',
            },
            {
                title: 'Kontak',
                dataIndex: 'staff_family_contact',
                key: 'staff_family_contact'
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 160,
                render: (test, record) => (

                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit
                                record={record}
                                list={this.list}
                                formLayout={formLayout}/>
                        </span>
                        <span className={menuDelete.class}>
                            <Delete
                                record={record}
                                list={this.list}/>
                        </span>
                    </Button.Group>

                )
            }
        ];
        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['staff_family_id'],
                no: key + 1,
                ...item
            }
        });

        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <div className="user-general-info-wrapper">
                                    <span className={menuCreate.class}>
                                        <Create
                                            list={this.list}
                                            staff_id={this.state.staff_id}
                                            formLayout={formLayout}/>
                                    </span>
                                    <Table
                                        className={menuList.class}
                                        columns={columns}
                                        bordered={true}
                                        loading={this.state.isLoading}
                                        dataSource={dataList}
                                        size='small'/>


                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }

    list = () => {
        this.setState({
            isLoading: true
        });

        GetData(`/staff-keluarga/${this.state.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 19,
        offset: 5
    },
    'lg': {
        span: 19,
        offset: 5
    }
};
