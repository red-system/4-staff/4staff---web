import {Button, Icon, message, Popconfirm} from "antd";
import React, {Component} from "react";
import {DeleteData, PostData} from "../../../../services/api";

class Delete extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            record: props.record
        }
    }

    render() {

        const text = 'Publish pelatihan ini ?';

        return (
            <Popconfirm
                placement="rightTop"
                title={text}
                onConfirm={this.confirm()}
                okText="Yes"
                cancelText="No"
            >
                <Button type="primary" icon={"eye"} loading={this.state.isLoading}>
                    Publish
                </Button>
            </Popconfirm>
        )
    }

    confirm = () => e => {
        const props = this.props;
        const hide = message.loading('Action in progress..', 0);
        this.setState({
            isLoading: true
        });
        let data = {
            learning_development_id: this.state.record.learning_development_id,
            learningd_publish: 'yes'
        };
        PostData(`/learning-development-publish`, data)
            .then((result) => {
                props.listRefreshRun(true);
                message.success(result.data.message);
                this.setState({isLoading: false});
                setTimeout(hide, 0);
            });
    }
}


export default Delete;