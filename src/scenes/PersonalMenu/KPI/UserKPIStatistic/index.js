import React, {Component} from 'react';
import ReactEcharts from "echarts-for-react";
import {Form, Input, Modal, Select, Spin, Divider} from "antd";
import {GetData, PostData} from "../../../../services/api";
import {menuActionViewAccess, yearToDay} from "../../../../services/app/General";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            staff: null,
            yearList: [],
            kpiData: [],
            isLoading: true,
            formValue: {
                kpis_year: yearToDay()
            }
        }
    }

    componentWillMount() {
        this.yearList();
        this.kpiStatistik();
    }

    render() {

        let menuList = menuActionViewAccess('personal_menu', 'kpi', 'statistic_kpi');

        let bar1 = {};

        bar1.option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['Persentase KPI'],
                textStyle: {
                    color: CHARTCONFIG.color.text
                }
            },
            toolbox: {
                show: true,
                feature: {
                    saveAsImage: {show: true, title: 'save'}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    data: ['Januari', 'Februari', 'Maret', 'April.', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'],
                    axisLabel: {
                        textStyle: {
                            color: CHARTCONFIG.color.text
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: CHARTCONFIG.color.splitLine
                        }
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLabel: {
                        textStyle: {
                            color: CHARTCONFIG.color.text
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: CHARTCONFIG.color.splitLine
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: CHARTCONFIG.color.splitArea
                        }
                    }
                }
            ],
            series: [
                {
                    name: 'Persentase KPI',
                    type: 'line',
                    stack: 'Sum',
                    data: this.state.kpiData,
                    markPoint: this.state.kpiData,
                    itemStyle: {
                        normal: {
                            color: CHARTCONFIG.color.primary,
                            areaStyle: {
                                color: CHARTCONFIG.color.primary,
                                type: 'default'
                            }
                        }
                    },
                }
            ]
        };


        return (
            <div className={menuList.class}>

                <Form {...formItemLayout}>
                    <Form.Item
                        label="Tahun KPI"
                    >
                        <Select
                            showSearch
                            placeholder="Pilih Staf"
                            optionFilterProp="children"
                            defaultValue={this.state.formValue.kpis_year}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            onChange={this.onChange('kpis_year')}>
                            {
                                this.state.yearList.map((item, key) => {
                                    return (
                                        <Select.Option key={key} value={item.kpis_year}>{item.kpis_year}</Select.Option>
                                    )
                                })
                            }
                        </Select>
                    </Form.Item>
                </Form>
                <Divider dashed/>
                <Spin spinning={this.state.isLoading}>
                    <ReactEcharts option={bar1.option} theme={"macarons"} width={"100%"}/>
                </Spin>
            </div>
        )
    }

    yearList() {
        GetData('/kpi-year-list').then((result) => {
            this.setState({
                yearList: result.data.data
            });
        });
    }

    kpiStatistik(kpis_year = null) {
        this.setState({
            isLoading: true
        });

        let data = {
            kpis_year: kpis_year === null ? this.state.formValue.kpis_year : kpis_year
        };
        PostData('/kpi-statistik', data).then((result) => {
            this.setState({
                kpiData: result.data.data,
                isLoading: false
            })
        });
    }

    onChange = name => value => {
        if (typeof value == 'object') {
            value = value.target.value;
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });

        this.kpiStatistik(value);
    };

}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 5},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 19},
    },
};

const CHARTCONFIG = {
    color: {
        primary: 'rgba(16,142,233,.85)',  // #108ee9
        success: 'rgba(61,189,125,.85)',  // #3dbd7d
        info: 'rgba(1,188,212,.85)',   // #01BCD4
        infoAlt: 'rgba(148,138,236,.85)', // #948aec
        warning: 'rgba(255,206,61,.85)',  // #ffce3d
        danger: 'rgba(244,110,101,.85)', // #f46e65
        gray: 'rgba(221,221,221,.3)',
        text: '#898989',         // for dark theme as well
        splitLine: 'rgba(0,0,0,.05)',
        splitArea: ['rgba(250,250,250,0.035)', 'rgba(200,200,200,0.1)'],
    }
};

