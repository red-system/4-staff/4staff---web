import React, {Component} from 'react';
import {Table, Button} from "antd";
import {GetData, GenerateLink} from "../../../../services/api";
import GENERALDATA from "../../../../constants/generalData";
import {menuActionViewAccess} from "../../../../services/app/General";

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    render() {
        let loginData = JSON.parse(localStorage.getItem('loginData'));
        let staff_id = loginData.staff_id;
        let menuList = menuActionViewAccess('personal_menu', 'kpi', 'period_kpi_list');
        let menuPreview = menuActionViewAccess('personal_menu', 'kpi', 'period_kpi_preview');


        const columns = [
            {
                title: 'Nama Periode KPI',
                dataIndex: 'kpi_periode_title',
                key: 'kpi_periode_title',
            },
            {
                title: 'Tahun Periode',
                dataIndex: 'kpi_periode_year',
                key: 'kpi_periode_year',
            },
            {
                title: 'Bulan Periode',
                dataIndex: 'kpi_periode_month_view_label',
                key: 'kpi_periode_month_view_label',
            },
            {
                title: 'Deskripsi',
                dataIndex: 'kpi_periode_description',
                key: 'kpi_periode_description'
            },
            {
                title: 'Menu',
                width: 80,
                render: (text, record) => {
                    return (
                        <a className={menuPreview.class}
                           href={GenerateLink('/kpi-period-staff/' + record.kpi_periode_id + '/' + staff_id)}
                           target={"_blank"}>
                            <Button type={"success"} icon={"file-pdf"} size={"small"}>View Periode KPI</Button>
                        </a>
                    )
                }
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item
            }
        });
        return <Table
            className={menuList.class}
            columns={columns}
            bordered={true}
            dataSource={dataList}
            size='small'/>
    }

    list() {
        GetData('/kpi-period-list-staff').then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}