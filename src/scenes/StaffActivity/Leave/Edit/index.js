import React, {Component} from 'react';
import {Button, Form, Icon, Input, DatePicker, Modal, Row, Col, InputNumber, Select, message} from "antd";
import 'react-quill/dist/quill.snow.css';
import {GetData, PostData} from "../../../../services/api";
import moment from "moment";
import LeaveManageDetil from "../../../StaffAttendence/Leave/4_0_LeaveManage_Detail";
import {dateFormatApp, dateFormatterApp} from "../../../../services/app/General";

let {Option} = Select;
const {TextArea} = Input;

export default class Edit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: props.modalView,
            pengumuman: '',
            master: [],
            formValue: props.record,
            regulation: [],
            attachment: [],
            regulation_label: "",
            attachment_field: "no",
            formError: [],
            staff: [],
            replace_field: "no",
            row_leave_master: {
                '': {}
            }
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            modalCreateVisible: nextProps.modalView,
            formValue: {
                ...this.state.formValue,
                leave_staff_attachment: undefined,
            },
            attachment: nextProps.attachment,
            regulation: nextProps.regulation,
            replace: nextProps.replace,
            master: nextProps.master,
            regulation_label: nextProps.regulation[this.state.formValue.leave_master_id],
            attachment_field: nextProps.attachment[this.state.formValue.leave_master_id],
            replace_field: nextProps.replace[this.state.formValue.leave_master_id],
            staff: nextProps.staff
        });
        this.onChangeJenis(this.state.formValue.leave_master_id);

        let leave_master = [];
        nextProps.master.map((item, key) => {
            leave_master[item['leave_master_id']] = item;
        });

        this.setState({
           row_leave_master : leave_master
        });
    }

    handleChange(value) {
        this.setState({pengumuman: value})
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateSubmit = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {

        console.log(this.state.row_leave_master);

        const {modalCreateVisible} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };

        return (
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Pengajuan Cuti
                        </span>
                }
                visible={modalCreateVisible}
                onOk={this.modalCreateSubmit}
                onCancel={this.modalCreateCancel}
                footer={[
                    <Row style={{textAlign: 'left'}}>
                        <Col {...formLayout}>
                            <Button key="submit" type="primary" onClick={this.insert}>
                                Simpan
                            </Button>
                            <Button key="back" onClick={this.modalCreateCancel}>
                                Batal
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Tanggal Cuti"
                        {...this.state.formError.leave_staff_date}
                    >
                        <DatePicker
                            style={{width: '100%'}}
                            onChange={this.onChangeDate("leave_staff_date")}
                            value={moment(dateFormatterApp(this.state.formValue.leave_staff_date), dateFormatApp())}
                            format={dateFormatApp()}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Jenis Cuti"
                        {...this.state.formError.staff_master_id}
                    >

                        <Select
                            showSearch
                            style={{width: 200}}
                            placeholder="Pilih Jenis Cuti"
                            optionFilterProp="children"
                            onChange={this.onChangeJenis()}
                            value={this.state.formValue.leave_master_id}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }>
                            {
                                Object.keys(this.state.master).map((key) => {
                                    return (
                                        <Option key={key}
                                                value={this.state.master[key].leave_master_id}>{this.state.master[key].leavem_name}</Option>
                                    )
                                })
                            }

                        </Select>
                        &nbsp;
                        <LeaveManageDetil record={this.state.row_leave_master[this.state.formValue.leave_master_id]}/>
                    </Form.Item>
                    <Form.Item
                        label="Durasi"
                        {...this.state.formError.leave_staff_duration}
                    >
                        <InputNumber
                            style={{"width": "100%"}}
                            value={this.state.formValue.leave_staff_duration == undefined ? "" : this.state.formValue.leave_staff_duration}
                            onChange={this.onChange("leave_staff_duration")}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Alasan Cuti"
                        {...this.state.formError.leave_staff_reason}
                    >
                        <TextArea
                            value={this.state.formValue.leave_staff_reason == undefined ? "" : this.state.formValue.leave_staff_reason}
                            onChange={this.onChange("leave_staff_reason")}
                        />
                    </Form.Item>
                    {this.state.replace_field === "yes" &&
                    <Form.Item
                        label="Staf Pengganti">
                        <Select
                            showSearch
                            style={{width: "100%"}}
                            placeholder="Pilih Staff"
                            optionFilterProp="children"
                            value={this.state.formValue.substitution_staff_id}
                            onChange={this.onChange("substitution_staff_id")}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {
                                Object.keys(this.state.staff).map((key) => {

                                    return (
                                        <Option key={key}
                                                value={this.state.staff[key]['staff_id']}>{this.state.staff[key]['staff_name']}</Option>
                                    )
                                })
                            }
                        </Select>

                    </Form.Item>
                    }
                    {this.state.attachment_field === "yes" &&
                    <Form.Item
                        label="Lampiran Cuti"
                    >
                        <div className="upload-btn-wrapper">
                            <button className="btn-file">Browse File</button>
                            <input type="file" name="myfile" onChange={this.handleChange}/>
                        </div>
                    </Form.Item>
                    }
                </Form>
            </Modal>
        )
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                leave_staff_attachment: event.target.files[0]
            }
        })
    };
    onChangeDate = name => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: moment(value).format("YYYY-MM-DD")
            }
        })
    };
    onChange = name => value => {
        if (typeof value == "object") {
            value = value.target.value
        }
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        })
    };
    onChangeJenis = () => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                leave_master_id: value
            },
            regulation_label: this.state.regulation[value],
            attachment_field: this.state.attachment[value],
            replace_field: this.state.replace[value],
        })

    };
    insert = (e) => {
        const formData = new FormData();
        Object.keys(this.state.formValue).map((key) => {
            if (this.state.formValue[key] != undefined) {
                formData.append(key, this.state.formValue[key])
            }
        });
        PostData("/leave/" + this.props.record.key, formData).then((result) => {
            let response = result.data;
            if (response.status === "success") {
                message.success(response.message)
                this.setState({
                    modalCreateVisible: false,
                });
                this.props.list()
            } else {
                let errors = response.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: 'error',
                        help: errors[key][0]
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false
                });
            }
        })
    }
}

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 16,
        offset: 8
    },
    'lg': {
        span: 16,
        offset: 8
    }
};