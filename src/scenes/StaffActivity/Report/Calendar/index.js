import React, {Component} from 'react';
import {Button, Table, Alert, Calendar, Badge, Icon, Row, Col, Form, Input, DatePicker, Modal} from "antd";
import moment from "moment";
import {dateFormatterApp, menuActionViewAccess} from "../../../../services/app/General";
import {PostData} from "../../../../services/api";


export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataOne: [],
            dataAll: [],
            dateSelect: null,
            modalVisible: false,
            formError: [],
            formValue: []
        }
    }

    componentWillMount() {
        this.reportListDateAll();
    }

    render() {
        let menuList = menuActionViewAccess('key_activity', 'report', 'list');
        let menuManage = menuActionViewAccess('key_activity', 'report', 'manage');

        let {dataAll} = this.state;

        function getListData(value) {
            let listData = [];
            let year = value.format('YYYY');
            let month = value.format('MM');
            let day = value.format('DD');
            let date = `${year}-${month}-${day}`;

            Object.keys(dataAll).map((key) => {
                if (key == date) {
                    dataAll[key].map((item, key2) => {
                        listData[key2] = {
                            type: 'success',
                            content: `${item['description']}`
                        }
                    });
                }
            });

            return listData || [];
        }

        function dateCellRender(value) {
            const listData = getListData(value);
            return (
                <ul className="events">
                    {listData.map(item => (
                        <li key={item.content}>
                            <Badge status={item.type} text={item.content}/>
                        </li>
                    ))}
                </ul>
            );
        }

        function getMonthData(value) {
            if (value.month() === 8) {
                return 1394;
            }
        }

        function monthCellRender(value) {
            const num = getMonthData(value);
            return num ? (
                <div className="notes-month">
                    <section>{num}</section>
                    <span>Backlog number</span>
                </div>
            ) : null;
        }

        return (
            <div>
                <Alert
                    message="Tips pengisian"
                    description="Laporan pekerjaan bisa diisi dengan klik tanggal pada kalender."
                    type="info"
                    showIcon
                />
                <Calendar className={menuList.class}
                          dateCellRender={dateCellRender}
                          onSelect={this.onSelect}
                          monthCellRender={monthCellRender}/>

                <Modal
                    className={menuManage.class}
                    title={
                        <span>
                            <Icon type="calendar"/> Laporan {dateFormatterApp(this.state.dateSelect)}
                        </span>
                    }
                    onCancel={this.modalStatus(false)}
                    visible={this.state.modalVisible}
                    width={700}
                    footer={[
                        <Row style={{textAlign: 'left'}}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.reportInsert}>
                                    Simpan
                                </Button>
                                <Button key="back" onClick={this.modalStatus(false)}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                    <Form {...formItemLayout}>
                        {
                            this.state.dataOne.map((item, key) => {
                                console.log('loop', item['description']);
                                return (
                                    <Form.Item
                                        label="Item Pekerjaan"
                                    >
                                        <Input.TextArea
                                            rows={2}
                                            value={item['description']}
                                            style={{width: "90%"}}
                                            onChange={this.onChangeDescription(key)}/>
                                        <Button type={"danger"} icon={"close"} onClick={this.itemDelete(key)}/>
                                    </Form.Item>
                                )
                            })
                        }
                        <Form.Item
                            label="&nbsp;"
                        >
                            <Button type={"success"} icon={"plus"} onClick={this.itemAdd}>Tambah item pekerjaan</Button>
                        </Form.Item>
                    </Form>

                </Modal>
            </div>
        )
    }

    onSelect = value => {
        this.reportListDateOne(value.format('YYYY-MM-DD'));
        this.setState({
            dateSelect: value.format('YYYY-MM-DD'),
            modalVisible: true
        });
    };

    onChangeDescription = key => e => {
        let value = e.target.value;
        let dataOne = this.state.dataOne;
        dataOne[key] = {
            description: value
        };

        console.log('change', key, value, dataOne);

        this.setState({
            dataOne: dataOne
        });
    };

    reportInsert = (e) => {

        let description = {};

        this.state.dataOne.map((item, key) => {
            description[key] = item['description'];
        });

        let data = {
            description: description,
            date: this.state.dateSelect
        };

        PostData('/report', data).then((result) => {
            this.setState({
                modalVisible: false
            });

            this.reportListDateAll();
        });
    };

    itemAdd = () => {

        let dataOne = this.state.dataOne;

        console.log('before very', dataOne);

        let no = 0;
        let data_new = dataOne.map((item, key) => {
            //console.log(key, item);
            no = key;
            return item;
        });

        console.log('before', no, data_new);

        no = parseInt(no) + 1;

        data_new[no] = "";

        console.log('after', no, data_new);


        this.setState({
            dataOne: data_new
        });
    };

    itemDelete = (key_delete) => e => {
        let dataOne = this.state.dataOne;

        delete dataOne[key_delete];

        this.setState({
            dataOne: dataOne
        });

        // let data = {
        //     description: this.state.dataOne,
        //     date: this.state.dateSelect
        // };
        //
        // PostData('/report', data).then((result) => {
        //     this.setState({
        //
        //     })
        // });
    };

    reportListDateOne = (date) => {
        let loginData = JSON.parse(localStorage.getItem('loginData'));
        let data = {
            staff_id: loginData.staff_id,
            date: date
        };
        PostData('/report-list-date-one', data).then((result) => {
            let dataOne = result.data.data.map((item, key) => {
                return item;
            });


            this.setState({
                dataOne: dataOne
            })
        });
    };

    reportListDateAll = () => {
        let loginData = JSON.parse(localStorage.getItem('loginData'));
        let data = {
            staff_id: loginData.staff_id,
        };
        PostData('/report-list-date-all', data).then((result) => {
            this.setState({
                dataAll: result.data.data
            })
        });
    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
    };
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

const formLayout = {
    'xs': {
        span: 24
    },
    'sm': {
        span: 24
    },
    'md': {
        span: 18,
        offset: 6
    },
    'lg': {
        span: 18,
        offset: 6
    }
};


