import React, {Component} from 'react';
import {Button, Col, Form, Icon, Input, Modal, Row, Radio, Spin, message, Popover} from "antd";
import {PostData} from "../../../services/api";

const {TextArea} = Input;

class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formLayout: props.formLayout,
            isLoading: true,
            dataPeriods: [],
            dataStaffIndicator: [],
            kpis_revise_reason:'',
        };
    }
    componentWillMount() {
        this.load()
    }


    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({numPages}) => {
        this.setState({numPages});
    };

    render() {

        const {modalEditVisible, formLayout} = this.state;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };
        const content = (
            <div>
                <TextArea onChange={this.onChange()}/>
                <div style={{marginTop: "10px"}} className={"text-center"}>
                    <Button type={"success"} size={"small"} onClick={this.sendReject()}><Icon type={"check"}/> Kirim</Button>
                </div>
            </div>
        );

        return (
            <span>
                <Button type="primary" icon="info-circle" onClick={this.modalEditShow}>
                    Detil Approval
                </Button>


                <Modal
                    title={"Detail Approval"}
                    visible={modalEditVisible}
                    onOk={this.modalEditSubmit}
                    onCancel={this.modalEditCancel}
                    width={1000}
                    footer={[

                        <Row style={{textAlign: 'center'}} key="1">
                            <Col {...formLayout}>
                                <Button key="back" type={"primary"} onClick={this.approve()}>
                                    <Icon type="check" /> Approve
                                </Button>
                                <Popover content={content} title="Alasan Ditolak" trigger="click">
                                    <Button key="back" type={"warning"}>
                                        <Icon type="rollback" /> Revise
                                    </Button>
                                </Popover>

                                <Button key="back" onClick={this.modalEditCancel}>
                                    <Icon type="close" /> Tutup
                                </Button>
                            </Col>
                        </Row>
                    ]}
                >
                     <table className="table-bordered">
                    <thead className={"ant-table-thead"}>
                    <tr>
                        <th>Item</th>
                        <th className="text-center">Weight</th>
                        <th>Indicator</th>
                        <th className="text-center">Weight</th>
                        <th className="text-center">Result</th>
                    </tr>
                    </thead>
                         {
                             this.state.dataStaffIndicator.map((item, key) => {

                                 const sub_indicator_first = item['sub_indicator_first'];
                                 let indicator_name = '-';
                                 let indicator_weight = 0;
                                 let kpisr_percent = '0';
                                 if (typeof sub_indicator_first[0] !== 'undefined') {
                                     indicator_name = sub_indicator_first[0]['indicator_name'];
                                     indicator_weight = sub_indicator_first[0]['indicator_weight'];
                                     kpisr_percent = sub_indicator_first[0]['kpisr_percent'];
                                 }


                                 return (
                                     <tbody key={key}>
                                     <tr key={key}>
                                         <td rowSpan={item['sub_indicator_count']}>{item['indicator_name']}</td>
                                         <td
                                             rowSpan={item['sub_indicator_count']}
                                             className="text-center">{item['indicator_weight']}%
                                         </td>
                                         <td>{indicator_name}</td>
                                         <td
                                             className="text-center">{indicator_weight}%
                                         </td>
                                         <td className="text-center">{kpisr_percent}%</td>
                                     </tr>
                                     {
                                         item['sub_indicator_others'].map((item2, key2) => {
                                             return (
                                                 <tr key={key2}>
                                                     <td>{item2['indicator_name']}</td>
                                                     <td
                                                         className="text-center">{item2['indicator_weight']}%
                                                     </td>
                                                     <td className="text-center">{item2['kpisr_percent']}%</td>
                                                 </tr>
                                             );
                                         })
                                     }
                                     </tbody>
                                 )
                             })
                         }
                </table>
                </Modal>

            </span>
        )
    }
    approve=()=>e=>{
        let kpi_staff_id = this.props.kpi_staff_id;
        const formData = new FormData();
        formData.append("kpi_staff_id",kpi_staff_id)
        PostData("/approval-approve",formData).then((result)=>{
            let data = result.data
            if(data.status="success"){
                this.props.list()
                this.setState({
                    modalEditVisible:false
                })
                message.success(data.message)
            } else {
                message.error(data.message)
            }

        })
    }
    sendReject=()=>e=>{
        let kpi_staff_id = this.props.kpi_staff_id;
        const formData = new FormData();
        formData.append("kpi_staff_id",kpi_staff_id)
        formData.append("kpis_revise_reason",this.state.kpis_revise_reason)
        PostData("/approval-revise",formData).then((result)=>{
            let data = result.data
            if(data.status="success"){
                this.props.list()
                this.setState({
                    modalEditVisible:false
                })
                message.success(data.message)
            } else {
                message.error(data.message)
            }

        })
    }
    load(){
        let kpi_staff_id = this.props.kpi_staff_id;
        let data = {
            kpi_staff_id: kpi_staff_id
        };
        this.setState({
            isLoading: true
        });
        PostData('/kpi-indicator/overview/kpi-result/edit', data)
            .then((result) => {
                this.setState({
                    dataStaffIndicator: result.data.data,
                    isLoading: false
                })
            });
    }
    onChange=()=>value=>{
        this.setState({
            kpis_revise_reason:value
        })
    }
}

export default ViewFile;