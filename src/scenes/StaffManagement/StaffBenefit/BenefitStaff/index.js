import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";

import Edit from "../BenefitStaffEdit";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            listRefresh: true,
            data: [],
            dataBenefit: props.dataBenefit,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.setState({
            dataBenefit: this.props.dataBenefit,
        });
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            dataBenefit: nextProps.dataBenefit,
        });
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (nextState.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            const nameMatch = get(record, "staff_name").match(reg);
            var departmentMatch = false;
            if (get(record, "department_title") != null) {
                departmentMatch = get(record, "department_title").match(reg);
            }
            const positionMatch = get(record, "structure_name").match(reg);
            const startWorkMatch = get(record, "staff_work_start").match(reg);
            if (!nameMatch && !departmentMatch && !positionMatch && !startWorkMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "staff_benefit", "list_benefit_staff");
        let menuEdit = menuActionViewAccess("staff_management", "staff_benefit", "edit_benefit_staff");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staff",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Departemen",
                dataIndex: "department_title",
                key: "department_title",
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Masa Kerja",
                dataIndex: "staff_work_start",
                key: "staff_work_start",
            },
            {
                title: "Daftar Manfaat",
                dataIndex: "benefit_render",
                key: "benefit_render",
                render: (benefit_render) => (
                    <span>
                        {benefit_render.map((item, key) => {
                            return (
                                <span key={key}>
                                    <Tag color={"green"} key={item["benefit_id"]}>
                                        {item["benefit_title"]}
                                    </Tag>
                                    <br />
                                </span>
                            );
                        })}
                    </span>
                ),
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 70,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuEdit.class}>
                            <Edit record={record} dataBenefit={this.state.dataBenefit} listRefreshRun={this.props.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                benefit_id_json: item["benefit_id_json"],
                key: item["staff_id"],
                no: key + 1,
                staff_name: item["staff_name"],
                department_title: item["department_title"],
                structure_name: item["structure_name"],
                staff_work_start: item["staff_work_start"],
                benefit_render: item["benefit"],
            };
        });

        return (
            <div className={menuList.class}>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />
            </div>
        );
    }

    list() {
        GetData("/benefit-staff").then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false,
                dataSearched: result.data.data,
                listRefresh: false,
            });
        });
    }

    // listRefreshRun = (value) => {
    //     console.log('index', value);
    //     this.setState({
    //         listRefresh: value,
    //         isLoading: value,
    //     });
    // }
}

export default RewardGuide;
