import React, { Component } from "react";
import { Button, Icon, Modal, Row, Col, Table, Input } from "antd";
import { GetData } from "../../../../services/api";
import { map, get } from "lodash";
const Search = Input.Search;

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var familyName = false;
            if (get(record, "staff_family_name") != null) {
                familyName = get(record, "staff_family_name").toString().match(reg);
            }
            var familyRelation = false;
            if (get(record, "staff_family_relation") != null) {
                familyRelation = get(record, "staff_family_relation").toString().match(reg);
            }
            var familyBirthday = false;
            if (get(record, "staff_family_birthday") != null) {
                familyBirthday = get(record, "staff_family_birthday").toString().match(reg);
            }
            var familyContact = false;
            if (get(record, "staff_family_contact") != null) {
                familyContact = get(record, "staff_family_contact").toString().match(reg);
            }
            if (!familyName && !familyRelation && !familyBirthday && !familyContact) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Nama Keluarga",
                dataIndex: "staff_family_name",
                key: "staff_family_name",
            },
            {
                title: "Jenis Hubungan",
                dataIndex: "staff_family_relation",
                key: "staff_family_relation",
            },
            {
                title: "Ulang Tahun",
                dataIndex: "staff_family_birthday",
                key: "staff_family_birthday",
            },
            {
                title: "Kontak Keluarga",
                dataIndex: "staff_family_contact",
                key: "staff_family_contact",
            },
        ];
        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="usergroup-delete" />
                Daftar Keluarga
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="usergroup-delete" /> Daftar Keluarga - {this.state.record.staff_name}
                    </span>
                }
                visible={this.state.modalVisible}
                width={900}
                onOk={this.modalStatus(true)}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row>
                        <Col>
                            <Button key="back" type={"warning"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                Kembali
                            </Button>
                        </Col>
                    </Row>,
                ]}
            >
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table columns={columns} loading={this.state.isLoading} bordered={true} dataSource={dataList} size="small" />
            </Modal>,
        ];
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });

        if (value) {
            this.list();
        }
    };

    list = () => {
        GetData(`/staff-keluarga/${this.state.record.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    };
}

export default Create;
