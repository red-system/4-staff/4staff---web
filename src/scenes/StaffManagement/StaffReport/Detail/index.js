import React, {Component} from 'react';
import {Alert, Badge, Button, Calendar, Col, Form, Icon, Input, Modal, Row, Tabs} from "antd";

import GENERALDATA from "../../../../constants/generalData";
import Breadcrumb from '../../../../components/Breadcrumb';
import CalendarView from "../Calendar";
import TableView from "../Table";


const {TabPane} = Tabs;

let pageNow = GENERALDATA.primaryMenu.staff_management.sub_menu["6"];
let pageParent = GENERALDATA.primaryMenu.staff_management;



export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            breadcrumbComponent: null,
            staff: props.location.state.record,
        };
    }

    componentWillMount() {
        const breadcrumb = [
            {
                label: pageParent.label,
                route: pageParent.route
            },
            {
                label: pageNow.label,
                route: `../${pageNow.route}`
            },
            {
                label: this.state.staff.staff_name,
                route: ''
            }
        ];

        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        })

    }

    render() {


        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>

                        <div className="card-container">
                            <Tabs type="card" defaultActiveKey="panduan-reward">
                                <TabPane tab={<span><Icon type="ordered-list"/> Tampilan Kalender</span>}
                                         key="kalender">
                                    <CalendarView staff={this.state.staff}/>
                                </TabPane>
                                <TabPane tab={<span><Icon type="gift"/> Tampilan Tabel</span>} key="tabel">
                                    <TableView staff={this.state.staff} />
                                </TabPane>
                            </Tabs>
                        </div>

                    </Col>
                </Row>
            </div>
        )
    }
}