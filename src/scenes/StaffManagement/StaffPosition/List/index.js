import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Edit from "../Edit";
import { GetData } from "../../../../services/api";
import { map, get } from "lodash";
import { menuActionViewAccess } from "../../../../services/app/General";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: props.isLoading,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            const nameMatch = get(record, "staff_name").match(reg);
            const departmentMath = get(record, "position.department.department_title").match(reg);
            const positionMatch = get(record, "position.structure_name").match(reg);
            if (!nameMatch && !departmentMath && !positionMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);
        console.log("filteredData: " + filteredData);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "staff_position", "list");
        let menuEdit = menuActionViewAccess("staff_management", "staff_position", "edit");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", key: "no", width: 40 },
            { title: "Staf", dataIndex: "staff_name", key: "staff_name" },
            { title: "Departemen", dataIndex: "position.department.department_title", key: "department_title" },
            { title: "Posisi", dataIndex: "position.structure_name", key: "structure_name" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, record) => {
                    return (
                        <Button.Group size={"small"}>
                            <span className={menuEdit.class}>
                                <Edit record={record} listRefreshRun={this.props.listRefreshRun} />
                            </span>
                        </Button.Group>
                    );
                },
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuList.class}
                    columns={columns}
                    dataSource={data}
                    onChange={this.handleChange}
                    bordered={true}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/staff-position").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
