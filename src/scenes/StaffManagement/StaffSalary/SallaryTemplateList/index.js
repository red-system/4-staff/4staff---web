import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import { GetData } from "../../../../services/api";
import SallaryTemplateEdit from "../SallaryTemplateEdit";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

class StaffSallary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var staffNameMatch = false;
            if (get(record, "staff_name")) {
                staffNameMatch = get(record, "staff_name").match(reg);
            }
            var positionMatch = false;
            if (get(record, "structure_name")) {
                positionMatch = get(record, "structure_name").match(reg);
            }
            var lastUpdateMatch = false;
            if (get(record, "last_update") != null) {
                lastUpdateMatch = get(record, "last_update").match(reg);
            }
            if (!staffNameMatch && !positionMatch && !lastUpdateMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListSalaryTemplateStaff = menuActionViewAccess("staff_management", "salary", "list_salary_template_staff");
        let menuEditSalaryTemplateStaff = menuActionViewAccess("staff_management", "salary", "edit_salary_template_staff");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Nama Staff", dataIndex: "staff_name", key: "staff_name" },
            { title: "Posisi Staff", dataIndex: "structure_name", key: "structure_name" },
            { title: "Last Update", dataIndex: "last_update", key: "last_update" },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 70,
                render: (text, record) => (
                    <span className={menuEditSalaryTemplateStaff.class}>
                        <SallaryTemplateEdit record={record} list={this.list} />
                    </span>
                ),
            },
        ];

        const dataList = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
                structure_name: item.position.structure_name,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuListSalaryTemplateStaff.class}
                    columns={columns}
                    dataSource={dataList}
                    bordered={true}
                    loading={this.state.isLoading}
                    tip={"loading"}
                    size="small"
                />
            </div>
        );
    }

    list = (e) => {
        GetData("/salary-template").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    };
}

export default StaffSallary;
