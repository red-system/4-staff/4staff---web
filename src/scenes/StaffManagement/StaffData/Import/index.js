import React, { Component } from "react";
import { Button, Form, Icon, Input, Modal, Upload, Row, Col, message } from "antd";
import { PostData } from "../../../../services/api";

const { TextArea } = Input;

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            isLoading: false,
            formLayout: props.formLayout,
            formError: [],
            formValue: {
                staff_import_description: "",
                staff_import_filename: "",
            },
        };
    }

    modalCreateShow = () => {
        this.setState({
            modalCreateVisible: true,
        });
    };

    modalCreateCancel = () => {
        this.setState({
            modalCreateVisible: false,
        });
    };

    render() {
        const { modalCreateVisible, formLayout } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };

        return (
            <span>
                <Button type="success" onClick={this.modalCreateShow}>
                    <Icon type="import" /> Import Data
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="plus" /> Import Data
                        </span>
                    }
                    visible={modalCreateVisible}
                    width={700}
                    onOk={this.insert}
                    onCancel={this.modalCreateCancel}
                    footer={[
                        <Row style={{ textAlign: "left" }}>
                            <Col {...formLayout}>
                                <Button key="submit" type="primary" onClick={this.insert}>
                                    Import
                                </Button>
                                <Button key="back" onClick={this.modalCreateCancel}>
                                    Batal
                                </Button>
                            </Col>
                        </Row>,
                    ]}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label="Contoh File Import">
                            <a href="/contoh-file-import-data-staff.xlsx" target="_blank">
                                Download Contoh Import Data Staff
                            </a>
                        </Form.Item>
                        <Form.Item label="Keterangan File Import" {...this.state.formError.staff_import_description}>
                            <TextArea onChange={this.onChange("staff_import_description")} />
                        </Form.Item>
                        <Form.Item label="File Absen" {...this.state.formError.staff_import_filename}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File Absence</button>
                                <input type="file" name="myfile" onChange={this.handleChange} />
                            </div>
                        </Form.Item>
                    </Form>
                </Modal>
            </span>
        );
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_import_filename: event.target.files[0],
            },
        });
    };

    onChange = (name) => (value) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });
        const formData = new FormData();

        formData.append("staff_import_description", this.state.formValue.staff_import_description);
        formData.append("staff_import_filename", this.state.formValue.staff_import_filename);

        PostData("/staff-import", formData).then((result) => {
            const data = result.data;

            if (data.result === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

export default Create;
