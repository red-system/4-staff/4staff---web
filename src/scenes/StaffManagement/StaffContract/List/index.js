import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Edit from "../Edit";
import Delete from "../Delete";
import Download from "../Download";
import { GetData } from "../../../../services/api";
import { map, get } from "lodash";
import { menuActionViewAccess } from "../../../../services/app/General";
const Search = Input.Search;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listRefreshRun: props.listRefreshRun,
            listRefresh: props.listRefresh,
            data: [],
            isLoading: true,
            dataStaff: [],
            dataRow: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }
    //
    // componentWillMount() {
    //     this.list();
    // }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }

        this.setState({
            dataStaff: nextProps.dataStaff,
            listRefreshRun: nextProps.listRefreshRun,
        });
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            const nameMatch = get(record, "staff_name").match(reg);
            const positionMatch = get(record, "structure_name").match(reg);
            const staffContractDateFromMatch = get(record, "staff_contract_date_from_format").match(reg);
            const staffContractDateToMatch = get(record, "staff_contract_date_to_format").match(reg);
            if (!nameMatch && !positionMatch && !staffContractDateFromMatch && !staffContractDateToMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("staff_management", "staff_contract", "list");
        let menuDownload = menuActionViewAccess("staff_management", "staff_contract", "view_file");
        let menuEdit = menuActionViewAccess("staff_management", "staff_contract", "edit");
        let menuDelete = menuActionViewAccess("staff_management", "staff_contract", "delete");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Staf",
                dataIndex: "staff_name",
                key: "staff_name",
            },
            {
                title: "Posisi Staf",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Tanggal Kontrak",
                dataIndex: "staff_contract_date_from_format",
                key: "staff_contract_date_from_format",
            },
            {
                title: "Tanggal Berakhir",
                dataIndex: "staff_contract_date_to_format",
                key: "staff_contract_date_to_format",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 250,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <span className={menuDownload.class}>
                            <Download record={record} />
                        </span>
                        <span className={menuEdit.class}>
                            <Edit record={record} listRefreshRun={this.state.listRefreshRun} dataStaff={this.state.dataStaff} />
                        </span>
                        <span className={menuDelete}>
                            <Delete record={record} listRefreshRun={this.state.listRefreshRun} />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: item["staff_contract_id"],
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table
                    className={menuList.class}
                    columns={columns}
                    dataSource={data}
                    onChange={this.handleChange}
                    bordered={true}
                    loading={this.state.isLoading}
                    size="small"
                />
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });

        GetData("/staff-contract").then((result) => {
            this.setState({
                data: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}

export default RewardGuide;
