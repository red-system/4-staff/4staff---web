import React, { Component } from "react";
import { Button, Form, Icon, Modal, Select, message } from "antd";
import { PostData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { log } from "async";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            formError: [],
            formValue: {
                staff_id: "",
                staff_filename: "",
            },
            listRefreshRun: props.listRefreshRun,
            dataStaff: [],

            department_title_state: "-",
            structure_name: "-",
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            dataStaff: nextProps.dataStaff,
        });
    }

    render() {
        const { dataStaff } = this.state;
        let menuCreate = menuActionViewAccess("staff_management", "staff_job_description", "create");

        return (
            <div className={menuCreate.class}>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus" /> Tambah Data
                </Button>
                <br />
                <br />

                <Modal
                    title={
                        <span>
                            <Icon type="plus" /> Tambah Data
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={700}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label="Staf" {...this.state.formError.staff_id}>
                            <Select
                                defaultValue={this.state.formValue.staff_id}
                                onChange={this.onChange("staff_id")}
                                showSearch
                                placeholder="Pilih Staf"
                                optionFilterProp="children"
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            >
                                {Object.keys(dataStaff).map((key) => {
                                    return (
                                        <Select.Option
                                            value={`${dataStaff[key]["staff_id"]}|${dataStaff[key].position.department.department_title ?? "-"}|${
                                                dataStaff[key].position.structure_name ?? "-"
                                            }`}
                                        >
                                            {dataStaff[key]["staff_name"]}
                                        </Select.Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Departemen">{this.state.department_title_state}</Form.Item>
                        <Form.Item label="Posisi">{this.state.structure_name}</Form.Item>
                        <Form.Item label="Upload File Deksripsi Pekerjaan" {...this.state.formError.staff_filename}>
                            <div className="upload-btn-wrapper">
                                <button className="btn-file">Browse File</button>
                                <input type="file" name="myfile" onChange={this.handleChange} />
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        );
    }

    handleChange = (event) => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_filename: event.target.files[0],
            },
        });
    };

    onChange = (name) => (value) => {
        if (typeof value == "object") {
            value = value.target.value;
        }

        if (name == "staff_id") {
            let valueArr = value.split("|");
            value = valueArr[0];
            let department_title_arr = valueArr[1];
            let structure_name_arr = valueArr[2];

            this.setState({
                department_title_state: department_title_arr,
                structure_name: structure_name_arr,
            });
        }

        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value,
            },
        });
    };

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
            });
        }
    };

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        const formData = new FormData();
        formData.append("staff_id", this.state.formValue.staff_id);
        formData.append("staff_filename", this.state.formValue.staff_filename);

        PostData("/staff-job-description-insert", formData).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                });
            } else {
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 10,
        },
    },
};

export default Create;
