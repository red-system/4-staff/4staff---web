import React, { Component } from "react";
import { Button, Icon, Modal, Row, Col, Table } from "antd";
import { GetData } from "../../../../services/api";
import Detail from "../Detail";
import moment from "moment";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            record: props.record,
            data: [],
            isLoading: true,
        };
    }

    render() {
        const sample = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        function getSample(value) {
            return sample[value];
        }

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Perusahaan",
                dataIndex: "staff_jh_company",
                key: "staff_jh_company",
            },
            {
                title: "Dari Bulan/Tahun",
                dataIndex: "staff_jh_date_from",
                key: "staff_jh_date_form",
                render: (date) => {
                    return getSample(moment(date).format("M")) + " / " + moment(date).format("YYYY");
                },
            },
            {
                title: "Sampai Bulan/Tahun",
                dataIndex: "staff_jh_date_to",
                key: "staff_jh_date_to",
                render: (date) => {
                    return getSample(moment(date).format("M")) + " / " + moment(date).format("YYYY");
                },
            },
            {
                title: "Jenis Usaha",
                dataIndex: "staff_jh_bussines_field",
                key: "staff_jh_bussines_field",
            },
            {
                title: "Menu",
                width: 100,
                render: (text, record) => {
                    return <Detail record={record} staff={this.state.record} />;
                },
            },
        ];
        const dataList = this.state.data.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="usergroup-delete" />
                Daftar Pekerjaan
            </Button>,
            <Modal
                title={
                    <span>
                        <Icon type="usergroup-delete" /> Daftar Pekerjaan - {this.state.record.staff_name}
                    </span>
                }
                visible={this.state.modalVisible}
                width={1000}
                onOk={this.modalStatus(true)}
                onCancel={this.modalStatus(false)}
                footer={[
                    <Row>
                        <Col>
                            <Button key="back" type={"warning"} icon={"rollback"} onClick={this.modalStatus(false)}>
                                Kembali
                            </Button>
                        </Col>
                    </Row>,
                ]}
            >
                <Table columns={columns} bordered={true} loading={this.state.isLoading} dataSource={dataList} size="small" />
            </Modal>,
        ];
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });

        if (value) {
            this.list();
        }
    };

    list = () => {
        GetData(`/staff-pekerjaan/${this.state.record.staff_id}`).then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false,
            });
        });
    };
}

export default Create;
