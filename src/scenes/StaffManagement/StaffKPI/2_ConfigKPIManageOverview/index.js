import React, {Component} from 'react';
import {Spin} from "antd";
import {PostData} from "../../../../services/api";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            data: [],
            isLoading: true,
        };
    }

    componentWillMount() {
        this.list();

        console.log('overview', this.props);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.listRefresh) {
            this.list();
        }
    }

    showModal = () => {
        this.setState({
            modalVisible: true,
        });
    };

    handleOk = (e) => {
        this.setState({
            modalVisible: false,
        });
    };
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            modalVisible: false,
        });
    };

    render() {


        return (
            <Spin tip={'Loading ...'} spinning={this.state.isLoading}>
                <table className="table-bordered">
                    <thead className={"ant-table-thead"}>
                    <tr>
                        <th>Item</th>
                        <th className="text-center">Weight</th>
                        <th>Indicator</th>
                        <th className="text-center">Weight</th>
                    </tr>
                    </thead>
                    {
                        this.state.data.map((item, key) => {

                            const sub_indicator_first = item['sub_indicator_first'];
                            let indicator_name = '-';
                            let indicator_weight = 0;
                            if (typeof sub_indicator_first[0] !== 'undefined') {
                                indicator_name = sub_indicator_first[0]['indicator_name'];
                                indicator_weight = sub_indicator_first[0]['indicator_weight'];
                            }


                            return (
                                <tbody key={key}>
                                <tr key={key}>
                                    <td rowSpan={item['sub_indicator_count']}>{item['indicator_name']}</td>
                                    <td
                                        rowSpan={item['sub_indicator_count']}
                                        className="text-center">{item['indicator_weight']}%
                                    </td>
                                    <td>{indicator_name}</td>
                                    <td
                                        className="text-center">{indicator_weight}%
                                    </td>
                                </tr>
                                {
                                    item['sub_indicator_others'].map((item2, key2) => {
                                        return (
                                            <tr key={key2}>
                                                <td>{item2['indicator_name']}</td>
                                                <td
                                                    className="text-center">{item2['indicator_weight']}%
                                                </td>
                                            </tr>
                                        );
                                    })
                                }
                                </tbody>
                            )
                        })
                    }
                </table>
            </Spin>
        )
    }

    list() {
        this.setState({
            isLoading: true
        });

        let data = {
            structure_id: this.props.structure.structure_id
        };
        PostData('/kpi-indicator/overview/kpi-result', data)
            .then((result) => {
                this.setState({
                    data: result.data.data,
                    isLoading: false
                })
            })
    }
}