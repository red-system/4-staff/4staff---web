import React, {Component} from 'react';
import {Button, Table} from "antd";

import Edit from "../2_ConfigKPIManageEdit";
import Delete from "../2_ConfigKPIManageDelete";
import SubIndicatorModalButton from "../2_ConfigKPIManageSubModal";
import {PostData} from "../../../../services/api";
import Create from "../2_ConfigKPIManageCreate";

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            structure: props.structure,
            dataIndicator: []
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (nextState.listRefresh) {
            this.list();
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.listRefresh) {
            this.list();
        }
    }

    render() {

        const columns = [
            {
                title: 'No',
                dataIndex: 'no',
                width: 30
            },
            {
                title: 'Nama Indikator',
                dataIndex: 'indicator_name',
                key: 'indicator_name'
            },
            {
                title: 'Bobot',
                dataIndex: 'indicator_weight_label',
                key: 'indicator_weight_label',
            },
            {
                title: 'Jumlah Sub Indikator',
                dataIndex: 'sub_indicator',
                key: 'sub_indicator',
            },
            {
                title: 'Menu',
                dataIndex: 'menu',
                key: 'menu',
                width: 330,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <SubIndicatorModalButton
                            kpi_indicator_parent_id={record.kpi_indicator_id}
                            indicator_name={record.indicator_name}
                            structure_id={this.state.structure.structure_id}
                            listRefreshRun={this.props.listRefreshRun}
                            listRefresh={this.props.listRefresh}/>
                        <Edit listRefreshRun={this.props.listRefreshRun}
                              structure={this.state.structure}
                              record={record}/>
                        <Delete record={record}
                                listRefreshRun={this.props.listRefreshRun}/>
                    </Button.Group>

                )
            }
        ];

        const data = this.state.dataIndicator.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                kpi_indicator_id: item['kpi_indicator_id'],
                kpi_indicator_parent_id: item['kpi_indicator_parent_id'],
                structure_id: this.state.structure.structure_id,
                indicator_name: item['indicator_name'],
                indicator_weight_label: `${item['indicator_weight']}%`,
                indicator_weight: `${item['indicator_weight']}`,
                sub_indicator: item['sub_indicator_count'],
            };
        });

        return (
            <div>
                <Create listRefreshRun={this.props.listRefreshRun}
                        structure={this.state.structure}/>
                <Table columns={columns}
                       dataSource={data}
                       bordered={true}
                       loading={this.state.isLoading}
                       size='small'/>
            </div>
        )
    }

    list = () => {
        this.setState({
           isLoading: true
        });

        let data = {
            'structure_id': this.state.structure.structure_id
        };
        PostData('/kpi-indicator/structure', data)
            .then((result) => {
                this.setState({
                    dataIndicator: result.data.data,
                    isLoading: false,
                    listRefresh: false
                })
            });
    };
}