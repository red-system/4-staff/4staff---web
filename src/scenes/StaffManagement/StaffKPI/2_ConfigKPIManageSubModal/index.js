import React from "react";
import { Modal, Button, Icon } from 'antd';

import SubIndicatorList from "../2_ConfigKPIManageSubList";

export default class AppComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
        }
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };

    render() {
        return (
            <span>
                <Button type="primary" onClick={this.showModal}>
                    <Icon type={"ordered-list"}/> Sub Indikator
                </Button>
                <Modal
                    title={<span><Icon type={"ordered-list"} /> Daftar Sub Indikator</span>}
                    visible={this.state.visible}
                    width={"85%"}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>
                            Tutup
                        </Button>,
                    ]}
                >
                    <SubIndicatorList
                        kpi_indicator_parent_id={this.props.kpi_indicator_parent_id}
                        indicator_name={this.props.indicator_name}
                        structure_id={this.props.structure_id}
                        listRefreshRun={this.props.listRefreshRun}/>
                </Modal>
            </span>
        );
    }
}