import React, { Component } from "react";
import { Button, Form, Icon, Modal, DatePicker, InputNumber, message, Select } from "antd";
import moment from "moment";
import { GetData, PostData } from "../../../../services/api";
import { monthToDay, strRandom, yearToDay } from "../../../../services/app/General";

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            dataIndicator: [],
            listStaff: props.listStaff,

            formValue: {
                staff_id: "",
                staff_name: "",
                structure_id: "",
                kpis_year: yearToDay(),
                kpis_month: monthToDay(),
                kpisr_percent: [],
            },
            formError: {},
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            listStaff: nextProps.listStaff,
        });
    }

    render() {
        return (
            <div>
                <Button type="primary" onClick={this.modalStatus(true)}>
                    <Icon type="plus" /> Membuat KPI Baru
                </Button>

                <Modal
                    title={
                        <span>
                            <Icon type="plus" /> KPI Baru
                        </span>
                    }
                    visible={this.state.modalVisible}
                    width={900}
                    onCancel={this.modalStatus(false)}
                    footer={null}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label="Staff" {...this.state.formError.staff_id}>
                            <Select
                                showSearch
                                placeholder="Staff"
                                optionFilterProp="children"
                                value={this.state.formValue.staff_id + "," + this.state.formValue.structure_id}
                                onSelect={this.onChange("staff_id")}
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                key={this.state.formValue.staff_id}
                            >
                                {this.state.listStaff.map((item, key) => {
                                    return (
                                        <Select.Option key={key} value={item.staff_id + "," + item.structure_id}>
                                            {item.staff_name}
                                        </Select.Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Tahun Bulan KPI Staff" {...this.state.formError.kpis_year}>
                            <DatePicker.MonthPicker
                                defaultValue={moment(this.state.formValue.kpis_year + "-" + this.state.formValue.kpis_month, "YYYY-MM")}
                                onChange={this.onChangeDate}
                                placeholder="Pilih bulan"
                            />
                        </Form.Item>
                        <Form.Item label="Persentase Indikator">
                            <table className="table-bordered">
                                <thead className={"ant-table-thead"}>
                                    <tr>
                                        <th>Item</th>
                                        <th className="text-center">Weight</th>
                                        <th>Indicator</th>
                                        <th className="text-center">Weight</th>
                                        <th className="text-center">Result</th>
                                    </tr>
                                </thead>
                                {this.state.dataIndicator.map((item, key) => {
                                    const sub_indicator_first = item["sub_indicator_first"];
                                    let indicator_name = "-";
                                    let indicator_weight = 0;
                                    let kpi_indicator_id = "-";
                                    if (typeof sub_indicator_first[0] !== "undefined") {
                                        kpi_indicator_id = sub_indicator_first[0]["kpi_indicator_id"];
                                        indicator_name = sub_indicator_first[0]["indicator_name"];
                                        indicator_weight = sub_indicator_first[0]["indicator_weight"];
                                    }

                                    return (
                                        <tbody key={key}>
                                            <tr key={key}>
                                                <td rowSpan={item["sub_indicator_count"]}>{item["indicator_name"]}</td>
                                                <td rowSpan={item["sub_indicator_count"]} className="text-center">
                                                    {item["indicator_weight"]}%
                                                </td>
                                                <td>{indicator_name}</td>
                                                <td className="text-center">{indicator_weight}%</td>
                                                <td className="text-center">
                                                    <InputNumber
                                                        min={0}
                                                        max={indicator_weight}
                                                        onChange={this.onChangeNumber(kpi_indicator_id)}
                                                        defaultValue={this.state.formValue.kpisr_percent[kpi_indicator_id]}
                                                        formatter={(value) => `${value}%`}
                                                        parser={(value) => value.replace("%", "")}
                                                    />
                                                </td>
                                            </tr>
                                            {item["sub_indicator_others"].map((item2, key2) => {
                                                let kpi_indicator_id = item2["kpi_indicator_id"];
                                                return (
                                                    <tr key={key2}>
                                                        <td>{item2["indicator_name"]}</td>
                                                        <td className="text-center">{item2["indicator_weight"]}%</td>
                                                        <td className="text-center">
                                                            <InputNumber
                                                                min={0}
                                                                max={item2["indicator_weight"]}
                                                                onChange={this.onChangeNumber(kpi_indicator_id)}
                                                                defaultValue={this.state.formValue.kpisr_percent[kpi_indicator_id]}
                                                                formatter={(value) => `${value}%`}
                                                                parser={(value) => value.replace("%", "")}
                                                            />
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    );
                                })}
                            </table>
                        </Form.Item>
                        <div className={"wrapper-button-float"}>
                            <Button key="submit" type="primary" htmlType={"submit"} icon={"check"} loading={this.state.isLoading} onClick={this.insert}>
                                Simpan
                            </Button>
                            &nbsp;
                            <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                                Batal
                            </Button>
                        </div>
                    </Form>
                </Modal>
            </div>
        );
    }

    modalStatus = (value) => (e) => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: [],
            });
        }
    };

    onChangeDate = (date, dateString) => {
        let kpis_year = moment(dateString).format("YYYY");
        let kpis_month = moment(dateString).format("MM");

        this.setState({
            formValue: {
                ...this.state.formValue,
                kpis_year: kpis_year,
                kpis_month: kpis_month,
            },
        });
    };

    onChange = (name) => (value) => {
        value = value.split(",");
        let staff_id = value[0];
        let structure_id = value[1];
        this.list(staff_id, structure_id);
        this.setState({
            formValue: {
                ...this.state.formValue,
                staff_id: staff_id,
                structure_id: structure_id,
            },
        });
    };

    onChangeNumber = (kpi_indicator_id) => (value) => {
        // console.log(kpi_indicator_id, value);
        let kpisr_percent = this.state.formValue.kpisr_percent;
        // let kpi_indicator_id_status = kpi_indicator_id_state.hasOwnProperty(kpi_indicator_id);

        kpisr_percent[kpi_indicator_id] = value;

        this.setState({
            formValue: {
                ...this.state.formValue,
                kpisr_percent: kpisr_percent,
            },
        });

        //console.log('change number', this.state.formValue.kpi_indicator_id);
    };

    list(staff_id, structure_id) {
        let data = {
            staff_id: staff_id,
            structure_id: structure_id,
        };

        PostData("/kpi-indicator/overview/kpi-result", data).then((result) => {
            const kpisr_percent = this.indicatorSplit(result.data.data);
            // console.log('percent', kpisr_percent);
            this.setState({
                dataIndicator: result.data.data,
                formValue: {
                    ...this.state.formValue,
                    kpisr_percent: kpisr_percent,
                },
            });
        });
    }

    /**
     * untuk mendapatkan kpi_indicator_id menjadi agar untuk inisiasi awal
     * @param dataIndicator
     * @returns {[]}
     */
    indicatorSplit(dataIndicator) {
        const kpi_data_indicator = [];
        dataIndicator.map((item, key) => {
            if (typeof item["sub_indicator_first"][0] !== "undefined") {
                let kpi_indicator_id_first = item["sub_indicator_first"][0].kpi_indicator_id.toString();
                kpi_data_indicator[kpi_indicator_id_first] = 0;
                item["sub_indicator_others"].map((item2, key) => {
                    let kpi_indicator_id_others = item2["kpi_indicator_id"].toString();
                    kpi_data_indicator[kpi_indicator_id_others] = 0;
                });
            }
        });

        return kpi_data_indicator;
    }

    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true,
        });

        PostData("/kpi-data/staff/process/insert", this.state.formValue).then((result) => {
            const data = result.data;
            if (data.status === "success") {
                this.props.listRefreshRun(true);
                message.success(data.message);
                this.setState({
                    isLoading: false,
                    modalVisible: false,
                    formError: [],
                });
            } else {
                message.warning(data.message);
                let errors = data.errors;
                let formError = [];

                Object.keys(errors).map(function (key) {
                    formError[`${key}`] = {
                        validateStatus: "error",
                        help: errors[key][0],
                    };
                });

                this.setState({
                    formError: formError,
                    isLoading: false,
                });
            }
        });

        return false;
    };
}

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
    },
};

export default Create;
