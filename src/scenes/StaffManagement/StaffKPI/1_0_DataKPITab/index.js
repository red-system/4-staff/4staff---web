import React, { Component } from "react";
import { Tabs, Icon } from "antd";

import DataKPIAll from "../1_1_DataKPIAll";
import KPICreate from "../1_0_DataKPICreate";
import DataKPIProcessList from "../1_2_DataKPIProcessList";
import DataKPIReviseList from "../1_3_DataKPIReviseList";
import DataKPIAcceptList from "../1_4_DataKPIAcceptList";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";

const { TabPane } = Tabs;

class RewardGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listStaff: [],
        };
    }

    componentWillMount() {
        this.listStaff();
    }

    render() {
        let menuCreateDataKpi = menuActionViewAccess("staff_management", "kpi", "create_data_kpi");

        return (
            <div>
                <span className={menuCreateDataKpi.class}>
                    <KPICreate listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} listStaff={this.state.listStaff} />
                </span>
                <br />
                <Tabs type="card" defaultActiveKey="1">
                    <TabPane
                        tab={
                            <span>
                                <Icon type="pic-center" />
                                Semua Data KPI
                            </span>
                        }
                        key="1"
                    >
                        <DataKPIAll listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} listStaff={this.state.listStaff} />
                    </TabPane>
                    <TabPane
                        tab={
                            <span>
                                <Icon type="loading" />
                                Menunggu Konfirmasi
                            </span>
                        }
                        key="2"
                    >
                        <DataKPIProcessList listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                    </TabPane>
                    <TabPane
                        tab={
                            <span>
                                <Icon type="close" />
                                Revisi
                            </span>
                        }
                        key="3"
                    >
                        <DataKPIReviseList listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                    </TabPane>
                    <TabPane
                        tab={
                            <span>
                                <Icon type="check" />
                                Diterima
                            </span>
                        }
                        key="4"
                    >
                        <DataKPIAcceptList listRefresh={this.props.listRefresh} listRefreshRun={this.props.listRefreshRun} />
                    </TabPane>
                </Tabs>
            </div>
        );
    }

    listStaff() {
        GetData("/staff").then((result) => {
            this.setState({
                listStaff: result.data.data,
            });
        });
    }
}

export default RewardGuide;
