import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Tag} from "antd";
import 'react-quill/dist/quill.snow.css';
import {PostData} from "../../../../services/api";
import Spin from "antd/es/spin";
import {status_view} from "../../../../services/app/General"; // ES6

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            isLoading: false,
            data: [],
            record: props.record
        };
    }

    render() {

        let status = status_view(this.state.record.kpis_status);

        let reason_field = '';
        if (this.state.record.kpis_status === 'revise') {
            reason_field = [
                <Form.Item
                    label="Waktu Revisi">
                    {this.state.record.kpis_revise_date}
                </Form.Item>,
                <Form.Item
                    label="Alasa Revisi">
                    {this.state.record.kpis_revise_reason}
                </Form.Item>,
            ];
        }

        return [
            <Button type="default" onClick={this.modalStatus(true)}>
                <Icon type="eye"/> Preview
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="eye"/> KPI Detail
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >

                <Form {...formItemLayout} className={"form-info"}>
                    <Form.Item
                        label="Nama">
                        {this.state.record.staff_name}
                    </Form.Item>
                    <Form.Item
                        label="Posisi">
                        {this.state.record.structure_name}
                    </Form.Item>
                    <Form.Item
                        label="Tahun">
                        {this.state.record.kpis_year}
                    </Form.Item>
                    <Form.Item
                        label="Bulan">
                        {this.state.record.kpis_month_label}
                    </Form.Item>
                    <Form.Item
                        label="Status">
                        {status}
                    </Form.Item>
                    <Form.Item
                        label="Hasil">
                        {this.state.record.kpi_result_value}
                    </Form.Item>
                    <Form.Item
                        label="Kategori">
                        {this.state.record.kpi_rc_name}
                    </Form.Item>
                    <Form.Item
                        label="Waktu Pembuatan">
                        {this.state.record.created_at}
                    </Form.Item>
                    {
                        reason_field
                    }
                </Form>
                <Spin spinning={this.state.isLoading}>
                    <table className="table-bordered">
                        <thead className={"ant-table-thead"}>
                        <tr>
                            <th>Item</th>
                            <th className="text-center">Weight</th>
                            <th>Indicator</th>
                            <th className="text-center">Weight</th>
                            <th className="text-center">Result</th>
                        </tr>
                        </thead>
                        {
                            this.state.data.map((item, key) => {

                                const sub_indicator_first = item['sub_indicator_first'];
                                let indicator_name = '-';
                                let indicator_weight = 0;
                                let kpisr_percent = '0';
                                if (typeof sub_indicator_first[0] !== 'undefined') {
                                    indicator_name = sub_indicator_first[0]['indicator_name'];
                                    indicator_weight = sub_indicator_first[0]['indicator_weight'];
                                    kpisr_percent = sub_indicator_first[0]['kpisr_percent'];
                                }


                                return (
                                    <tbody key={key}>
                                    <tr key={key}>
                                        <td rowSpan={item['sub_indicator_count']}>{item['indicator_name']}</td>
                                        <td
                                            rowSpan={item['sub_indicator_count']}
                                            className="text-center">{item['indicator_weight']}%
                                        </td>
                                        <td>{indicator_name}</td>
                                        <td
                                            className="text-center">{indicator_weight}%
                                        </td>
                                        <td className="text-center">{kpisr_percent}%</td>
                                    </tr>
                                    {
                                        item['sub_indicator_others'].map((item2, key2) => {
                                            return (
                                                <tr key={key2}>
                                                    <td>{item2['indicator_name']}</td>
                                                    <td
                                                        className="text-center">{item2['indicator_weight']}%
                                                    </td>
                                                    <td className="text-center">{item2['kpisr_percent']}%</td>
                                                </tr>
                                            );
                                        })
                                    }
                                    </tbody>
                                )
                            })
                        }
                    </table>
                </Spin>
            </Modal>
        ]

    }

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        } else {
            this.kpi_staff_list(this.state.record.kpi_staff_id);
        }
    };

    kpi_staff_list = (kpi_staff_id) => {
        let data = {
            kpi_staff_id: kpi_staff_id
        };
        this.setState({
            isLoading: true
        });
        PostData('/kpi-indicator/overview/kpi-result/edit', data)
            .then((result) => {
                this.setState({
                    data: result.data.data,
                    isLoading: false
                })
            });
    }
}


const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 6},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18},
    },
};

export default Create;