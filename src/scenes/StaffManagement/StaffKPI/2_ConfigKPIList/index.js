import React, { Component } from "react";
import { Button, Table, Input, Icon } from "antd";

import Manage from "../2_ConfigKPIManageModal";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class AppComponent extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            dataStructure: [],
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.listRefresh) {
            this.list();
        }
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.dataStructure, (record) => {
            var positionMatch = false;
            if (get(record, "structure_name") != null) {
                positionMatch = get(record, "structure_name").toString().match(reg);
            }
            if (!positionMatch) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.dataStructure,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.dataStructure,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuListConfigKpi = menuActionViewAccess("staff_management", "kpi", "list_config_kpi");
        let menuManagementConfigKpi = menuActionViewAccess("staff_management", "kpi", "management_config_kpi");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const columns = [
            {
                title: "No",
                dataIndex: "no",
                width: 30,
            },
            {
                title: "Posisi",
                dataIndex: "structure_name",
                key: "structure_name",
            },
            {
                title: "Menu",
                dataIndex: "menu",
                key: "menu",
                width: 100,
                render: (text, structure) => (
                    <Button.Group size={"small"}>
                        <span className={menuManagementConfigKpi.class}>
                            <Manage
                                structure={structure}
                                tabActive={"overview"}
                                listRefresh={this.props.listRefresh}
                                listRefreshRun={this.props.listRefreshRun}
                            />
                        </span>
                    </Button.Group>
                ),
            },
        ];

        const data = this.state.dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                structure_id: item["structure_id"],
                structure_name: item["structure_name"],
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuListConfigKpi.class} columns={columns} dataSource={data} bordered={true} loading={this.state.isLoading} size="small" />;
            </div>
        );
    }

    list() {
        this.setState({
            isLoading: true,
        });
        GetData("/structure-staff").then((result) => {
            this.setState({
                dataStructure: result.data.data,
                dataSearched: result.data.data,
                isLoading: false,
            });
        });
    }
}
