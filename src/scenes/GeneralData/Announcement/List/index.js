import React, {Component} from 'react';
import {Button, Table} from "antd";

import Edit from '../Edit';
import Delete from '../Delete';
import ViewFileAnnouncement from '../ViewFile';
import {GetData} from "../../../../services/api";

class Announcement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.listRefresh) {
            this.list();
        }
    }

    render() {

        const columns = [
            {title: 'No', dataIndex: 'no', width: 30},
            {title: 'Judul', dataIndex: 'announcement_title', key: 'announcement_title'},
            {title: 'Deskripsi', dataIndex: 'announcement_description', key: 'announcement_description'},
            {title: 'Tanggal Upload', dataIndex: 'created_at', key: 'created_at'},
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', width: 300,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <ViewFileAnnouncement record={record}/>
                        <Edit record={record} listRefreshRun={this.props.listRefreshRun}/>
                        <Delete record={record} listRefreshRun={this.props.listRefreshRun}/>
                    </Button.Group>

                )
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['announcement_id'],
                no: key + 1,
                ...item
            }
        });

        return <Table columns={columns}
                      bordered={true}
                      dataSource={dataList}
                      loading={this.state.isLoading}
                      size='small'/>
    }

    list() {
        GetData('/announcement').then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}


export default Announcement;