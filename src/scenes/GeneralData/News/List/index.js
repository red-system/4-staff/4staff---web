import React, {Component} from 'react';
import {Button, Table} from "antd";

import Edit from '../Edit';
import Delete from '../Delete';
import ViewFile from '../ViewFile';
import {GetData} from "../../../../services/api";
import renderHtml from "react-render-html";

class News extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: []
        }
    }

    componentWillMount() {
        this.list();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.listRefresh) {
            this.list();
        }
    }

    render() {

        const columns = [
            {title: 'No', dataIndex: 'no', width: 30},
            {title: 'Judul', dataIndex: 'news_title', key: 'news_title'},
            {title: 'Deskripsi', dataIndex: 'news_description', key: 'news_description',className:"th-350"},
            {title: 'Tanggal Upload', dataIndex: 'created_at', key: 'created_at'},
            {
                title: 'Menu', dataIndex: 'menu', key: 'menu', className:"th-350", width: 300,
                render: (text, record) => (
                    <Button.Group size={"small"}>
                        <ViewFile record={record}/>
                        <Edit record={record} listRefreshRun={this.props.listRefreshRun}/>
                        <Delete record={record} listRefreshRun={this.props.listRefreshRun}/>
                    </Button.Group>

                )
            }
        ];

        const dataList = this.state.data.map((item, key) => {
            return {
                key: item['news_id'],
                no: key + 1,
                ...item,
                news_description: renderHtml(item['news_description'])
            }
        });

        return <Table columns={columns}
                      dataSource={dataList}
                      bordered={true}
                      loading={this.state.isLoading}
                      size='small'/>
    }

    list() {
        GetData('/news').then((result) => {
            this.setState({
                data: result.data.data,
                isLoading: false
            })
        });
    }
}


export default News;