import React, {Component} from 'react';
import {Col, Row} from "antd";

import List from './List';
import GENERALDATA from "../../../constants/generalData";
import Breadcrumb from '../../../components/Breadcrumb';
import {GetData} from "../../../services/api";

class RoleUser extends Component {

    constructor() {
        super();
        this.state = {
            breadcrumbComponent: null,
            isLoading: true,
            listRefresh: true,
            listRefreshRun: this.listRefreshRun,
            staff: [],
            roleMaster: [],
        };
    }

    componentWillMount() {

        this.setState({
            breadcrumbComponent: [(<Breadcrumb data={breadcrumb} key="0"/>)]
        });


        GetData('/role-permission').then((result) => {
            this.setState({
                roleMaster: result.data.data
            });
        });

    }

    render() {
        return (
            <div>
                {this.state.breadcrumbComponent}
                <Row>
                    <Col lg={24}>
                        <div className="app-content">
                            <div className="app-content-body">
                                <List {...this.state}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

    listRefreshRun = (value) => {
        this.setState({
            listRefresh: value
        })
    }



}

let pageNow = GENERALDATA.primaryMenu.general_data.sub_menu["4"];
let pageParent = GENERALDATA.primaryMenu.general_data;

const breadcrumb = [
    {
        label: pageParent.label,
        route: pageParent.route
    },
    {
        label: pageNow.label,
        route: `${pageNow.route}`
    },
];


export default RoleUser;