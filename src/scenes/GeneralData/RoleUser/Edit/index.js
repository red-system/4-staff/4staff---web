import React, {Component} from 'react';
import {Button, Form, Icon, Modal, Select, message} from "antd";
import {PostData} from "../../../../services/api";

const {Option} = Select;


class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            formValue: props.record,
            roleMaster: props.roleMaster,
            formError: []
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            roleMaster: nextProps.roleMaster
        })
    }

    render() {

        return [
            <Button type="success" onClick={this.modalStatus(true)}>
                <Icon type="edit"/>
                Edit
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="edit"/> Edit Posisi Staff
                        </span>
                }
                visible={this.state.modalVisible}
                width={700}
                onCancel={this.modalStatus(false)}
                footer={null}
            >
                <Form {...formItemLayout}>
                    <Form.Item
                        label="Staf">
                        {this.state.formValue.staff_name}
                    </Form.Item>
                    <Form.Item
                        label="Role & Permission"
                        {...this.state.formError.role_master_id}>

                        <Select
                            showSearch
                            placeholder="Role & Permission"
                            optionFilterProp="children"
                            value={this.state.formValue.role_master_id}
                            onSelect={this.onChange("role_master_id")}
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {
                                Object.keys(this.state.roleMaster).map((key) => {
                                    return (
                                        <Option key={key}
                                                value={this.state.roleMaster[key].role_master_id}>{this.state.roleMaster[key].role_master_title}</Option>
                                    )
                                })
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button key="submit" type="primary" htmlType={"submit"} icon={"check"}
                                loading={this.state.isLoading}
                                onClick={this.insert}
                        >
                            Simpan
                        </Button>
                        &nbsp;
                        <Button key="back" icon={"rollback"} onClick={this.modalStatus(false)}>
                            Batal
                        </Button>
                    </Form.Item>
                </Form>

            </Modal>
        ]
    }

    onChange = name => value => {
        this.setState({
            formValue: {
                ...this.state.formValue,
                [name]: value
            }
        });

    };

    modalStatus = value => e => {
        this.setState({
            modalVisible: value,
        });
        if (!value) {
            this.setState({
                isLoading: false,
                formError: []
            })
        }
    };
    insert = (e) => {
        e.preventDefault();

        this.setState({
            isLoading: true
        });

        PostData(`/role-user`, this.state.formValue)
            .then((result) => {
                const data = result.data;
                if (data.status === 'success') {
                    this.props.listRefreshRun(true);
                    message.success(data.message);
                    this.setState({
                        isLoading: false,
                        modalVisible: false,
                    });
                } else {
                    let errors = data.errors;
                    let formError = [];

                    Object.keys(errors).map(function (key) {
                        formError[`${key}`] = {
                            validateStatus: 'error',
                            help: errors[key][0]
                        };
                    });

                    this.setState({
                        formError: formError,
                        isLoading: false
                    });
                }
            });

        return false;
    }

}

const formItemLayout = {
    labelCol: {
        xs: {span: 24},
        sm: {span: 8},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 17,
            offset: 8,
        },
    },
};

export default Create;