import React, {Component} from 'react';
import {Button, Col, Icon, Modal, Row} from "antd";
import {PDFViewer, Page, Text, View, Document, StyleSheet} from '@react-pdf/renderer';
import {Link} from 'react-router-dom';


class ViewFile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalCreateVisible: false,
            modalEditVisible: false,
            numPages: null,
            pageNumber: 1,
            formLayout: props.formLayout,
        };
    }


    modalEditShow = () => {
        this.setState({
            modalEditVisible: true,
        });
    };

    modalEditSubmit = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    modalEditCancel = () => {
        this.setState({
            modalEditVisible: false,
        });
    };

    onDocumentLoad = ({numPages}) => {
        this.setState({numPages});
    };

    render() {

        const {modalEditVisible, formLayout} = this.state;


        return [
            <Button type="file" onClick={this.modalEditShow}>
                <Icon type="eye"/> View File
            </Button>,
            <Modal
                title={
                    <span>
                            <Icon type="eye"/> View File
                        </span>
                }
                visible={modalEditVisible}
                width={900}
                style={{top: 20}}
                onOk={this.modalEditSubmit}
                onCancel={this.modalEditCancel}
                footer={[

                    <Row style={{textAlign: 'center'}} key="1">
                        <Col {...formLayout}>
                            <a href={this.props.record.rulesr_filename_url} target="_blank">
                                <Button key="submit" type="primary" onClick={this.modalEditSubmit}>
                                    <Icon type="download"/> Download File
                                </Button>
                            </a>
                            &nbsp;

                            <Button key="back" type="warning" onClick={this.modalEditCancel}>
                                <Icon type="rollback"/> Batal
                            </Button>
                        </Col>
                    </Row>
                ]}
            >
                <object width="100%" height="700" data={this.props.record.rulesr_filename_url}
                        type="application/pdf"></object>

            </Modal>
        ]
    }
}

export default ViewFile;