import React, { Component } from "react";
import { Button, Table, Tag, Input, Icon } from "antd";
import { GetData } from "../../../../services/api";
import { menuActionViewAccess } from "../../../../services/app/General";
import { map, get } from "lodash";
const Search = Input.Search;

export default class RewardGuide extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isLoading: true,
            dataSearched: [],
            searchText: "",
            filteredInfo: null,
            sortedInfo: null,
            filtered: false,
        };

        this.list = this.list.bind(this);
    }

    componentWillMount() {
        this.list();
    }

    onInputChange = (e) => {
        this.setState({ searchText: e.target.value });
    };

    onSearch = (e) => {
        const reg = new RegExp(e.target.value, "gi");
        const filteredData = map(this.state.data, (record) => {
            var rewardTitle = false;
            if (get(record, "rewardg_title") != null) {
                rewardTitle = get(record, "rewardg_title").toString().match(reg);
            }
            var rewardDescription = false;
            if (get(record, "rewardg_description") != null) {
                rewardDescription = get(record, "rewardg_description").toString().match(reg);
            }
            var updatedAt = false;
            if (get(record, "updated_at") != null) {
                updatedAt = get(record, "updated_at").toString().match(reg);
            }
            if (!rewardTitle && !rewardDescription && !updatedAt) {
                return null;
            }
            return record;
        }).filter((record) => !!record);

        this.setState({
            searchText: e.target.value,
            filtered: !!e.target.value,
            dataSearched: e.target.value ? filteredData : this.state.data,
        });
    };

    emitEmpty = () => {
        this.setState({
            dataSearched: this.state.data,
            searchText: "",
        });
    };

    handleChange = (pagination, filters, sorter) => {
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };

    render() {
        let menuList = menuActionViewAccess("my_company", "reward_guide", "list");

        const { searchText } = this.state;
        const suffix = searchText ? <Icon type="close-circle" onClick={this.emitEmpty} styles={{ marginRight: 10 }} /> : null;

        const { dataSearched, isLoading } = this.state;

        const columns = [
            { title: "No", dataIndex: "no", width: 30 },
            { title: "Kinerja", dataIndex: "rewardg_title", key: "rewardg_title" },
            { title: "Penghargaan", dataIndex: "rewardg_description", key: "rewardg_description" },
        ];

        const dataList = dataSearched.map((item, key) => {
            return {
                key: key,
                no: key + 1,
                ...item,
            };
        });

        return (
            <div>
                <div className="row mb-1">
                    <div className="col-9"></div>
                    <div className="col-3 pull-right">
                        <Search
                            size="default"
                            ref={(ele) => (this.searchText = ele)}
                            suffix={suffix}
                            onChange={this.onSearch}
                            placeholder="Search Records"
                            value={this.state.searchText}
                            onPressEnter={this.onSearch}
                        />
                    </div>
                </div>
                <Table className={menuList.class} columns={columns} dataSource={dataList} bordered={true} size="small" loading={isLoading} />
            </div>
        );
    }

    list() {
        GetData("/reward-guide-staff").then((result) => {
            let data = result.data.data;
            this.setState({
                data: data,
                dataSearched: data,
                isLoading: false,
            });
        });
    }
}
