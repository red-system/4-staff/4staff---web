import React from 'react';
import QueueAnim from 'rc-queue-anim';
import {Form, Icon, Input, Button, Checkbox, Steps} from 'antd';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import forgotPasswordBackgroundForStaff from '../../images/login-background-forStaff.jpg';
import appLogo from '../../images/app-logo.png';

// 3rd
import '../../styles/antd.less';
import '../../styles/bootstrap/bootstrap.scss'
// custom
import "../../styles/layout.scss"
import "../../styles/theme.scss"
import "../../styles/ui.scss"
import "../../styles/vendors.scss"

import "../styles.scss"

// import Loadable from "react-loadable";
// import Loading from "../../components/Loading";

import StaffData from './1_StaffData';
import CompanyRules from './2_CompanyRules';
import JobDescription from './3_JobDescription';
import WorkFlow from './4_WorkFlow';
import Finish from './5_Finish';


const {Step} = Steps;


// const AsyncStaffData = Loadable({
//     loader: () => import('./components/1_StaffData'),
//     loading: Loading
// });
// const AsyncCompanyRules = Loadable({
//     loader: () => import('./components/2_CompanyRules'),
//     loading: Loading
// });
// const AsyncJobDescription = Loadable({
//     loader: () => import('./components/3_JobDescription'),
//     loading: Loading
// });
// const AsyncWorkFlow = Loadable({
//     loader: () => import('./components/4_WorkFlow'),
//     loading: Loading
// });

export default class AppComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listRefresh: true,
            match: props.match,
            staff_register_step: localStorage.getItem('staff_register_step'),
            staff_register_complete: localStorage.getItem('staff_register_complete'),
            loginStatus: localStorage.getItem('loginStatus'),
        }
    }

    // componentWillMount() {
    //     this.dataRegister();
    //     console.log('component will mount', this.state);
    // }
    //
    // componentWillUpdate(nextProps, nextState, nextContext) {
    //     console.log('will update', this.state, nextState);
    //     if(nextState.listRefresh) {
    //         this.dataRegister();
    //     }
    // }
    //
    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     console.log('did update', this.state, prevState);
    //     if(prevState.listRefresh) {
    //         this.dataRegister();
    //     }
    // }

    componentDidMount() {

    }

    render() {

        let {loginStatus, staff_register_complete, staff_register_step, match} = this.state;
        let url = this.props.location.pathname;

        if (!loginStatus || staff_register_complete == 'yes' || loginStatus == null || staff_register_complete == null) {
            return <Redirect to={""}/>
        }

        switch(url) {
            case "/register": staff_register_step = 1; break;
            case "/register/step-2": staff_register_step = 2; break;
            case "/register/step-3": staff_register_step = 3; break;
            case "/register/step-4": staff_register_step = 4; break;
            case "/register/finish": staff_register_step = 5; break;
        }


        return (

            <div key="1">
                <section className="register-container">
                    <div className="cover-bg-img-register">

                        <h3 style={{color: "white"}}>Register Staff di FORSTAFF</h3>
                        <p className="lead">Selamat Datang, Silahkan ikuti pengisian data beserta mempelajari
                            informasi tentang pekerjaan dan perusahaan.</p>

                    </div>
                    <div className="cover-form-container1">
                        <div className="col-12 col-md-10 col-lg-10 col-xl-10" style={{margin: "auto"}}>
                            <Steps current={staff_register_step - 1}>
                                <Step title="Data Diri" description="Isi Lengkap Data Diri"/>
                                <Step title="Peraturan Perusahaan" description="Tentang peraturan perusahaan."/>
                                <Step title="Job Description" description="Tetang deskripsi pekerjaan Anda"/>
                                <Step title="Alur Kerja" description="Tentang alur pekerjaan anda"/>
                            </Steps>
                            <br/>

                            <Route path={`${this.props.match.url}/step-2`} component={() => <CompanyRules changeStep={this.changeStep} match={this.props.match} />}/>
                            <Route path={`${this.props.match.url}/step-3`} component={() => <JobDescription changeStep={this.changeStep} match={this.props.match} />}/>
                            <Route path={`${this.props.match.url}/step-4`} component={() => <WorkFlow changeStep={this.changeStep} match={this.props.match} />}/>
                            <Route path={`${this.props.match.url}/finish`} component={() => <Finish />}/>
                            <Route path={`${this.props.match.url}`} component={() => <StaffData changeStep={this.changeStep} match={this.props.match} />} exact={true}/>

                        </div>
                    </div>
                </section>

            </div>
        )
    }

    changeStep = (step) => {
        this.setState({
            staff_register_step: step
        });
        localStorage.setItem('staff_register_step', step);
    }

    // dataRegister = () => {
    //     let loginStatus = localStorage.getItem("loginStatus");
    //     let staff_register_complete = localStorage.getItem("staff_register_complete");
    //     let staff_register_step = localStorage.getItem('staff_register_step');
    //
    //
    //     let step = this.state.step;
    //
    //     let stepContainer = {};
    //
    //     switch (step) {
    //         case 0:
    //             stepContainer = <1_StaffData listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //         case 1:
    //             stepContainer = <2_CompanyRules listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //         case 2:
    //             stepContainer = <3_JobDescription listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //         case 3:
    //             stepContainer = <4_WorkFlow listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //     }
    //
    //     this.setState({
    //         step: step,
    //         staff_register_complete: staff_register_complete,
    //         loginStatus: loginStatus,
    //         stepContainer: stepContainer,
    //         listRefresh: false
    //     });
    //
    //
    //
    //
    //     let step_2 = this.state.step;
    //
    //     let stepContainer_2 = {};
    //
    //     switch (step_2) {
    //         case 0:
    //             stepContainer_2 = <1_StaffData listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //         case 1:
    //             stepContainer_2 = <2_CompanyRules listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //         case 2:
    //             stepContainer_2 = <3_JobDescription listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //         case 3:
    //             stepContainer_2 = <4_WorkFlow listRefreshRun={this.listRefreshRun}/>;
    //             break;
    //     }
    //
    //     this.setState({
    //         step: step,
    //         staff_register_complete: staff_register_complete,
    //         loginStatus: loginStatus,
    //         stepContainer: stepContainer_2,
    //         listRefresh: false
    //     });
    // };

    // listRefreshRun = (value, step) => {
    //     this.setState({
    //         listRefresh: value,
    //         step: step
    //     })
    // }


}