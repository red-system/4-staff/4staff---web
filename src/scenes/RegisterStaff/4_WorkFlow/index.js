import React, {Component} from 'react';
import {Tabs, Icon, Button} from "antd";
import {GetData, PostData} from "../../../services/api";
import {Link, Redirect} from "react-router-dom";
import {renderFileType} from "../../../services/app/General";

const {TabPane} = Tabs;

export default class AppComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            redirect: false
        }
    }

    componentWillMount() {
        GetData('/work-flow')
            .then((result) => {
                this.setState({
                    data: result.data.data
                })
            });
    }


    render() {

        if(this.state.redirect) {
            return <Redirect to={"/register/finish"} />
        }


        return (
            <div>

                <div className={"btn-register-action"}>
                    <Button.Group size={"large"} style={{padding: "0"}}>
                        <Link to={"/register/step-3"} style={{padding: "0", margin: "0"}}>
                            <Button type="default" size={"large"}>
                                <Icon type="left"/>
                                Kembali
                            </Button>
                        </Link>
                        <Button type="primary" onClick={this.insert}>
                            Selesai
                            <Icon type="check"/>
                        </Button>
                    </Button.Group>
                </div>

                <Tabs type="card">
                    {
                        this.state.data.map((item, key) => {
                            return (
                                <TabPane tab={
                                    <span>
                                  <Icon type="snippets"/> {item['workf_title']}
                                </span>
                                } key={key}>
                                    <p style={{paddingLeft: 24, textAlign: "justify"}}>
                                        {item['workf_description']}
                                    </p>

                                    {renderFileType(item['workf_filename_type'], item['workf_filename_url'])}
                                </TabPane>
                            )
                        })
                    }
                </Tabs>
            </div>
        );
    }

    insert = (e) => {

        let loginData = localStorage.getItem('loginData');
        let staff_id = JSON.parse(loginData).staff_id;

        let data = {
            'staff_id': staff_id,
            'staff_register_step': 5
        };

        PostData('/staff-step', data)
            .then((result) => {
                this.setState({
                    redirect: true,
                });
                localStorage.setItem('staff_id_label', result.data.data.staff_id_label);
                localStorage.setItem('staff_register_complete', 'yes');
                this.props.changeStep(5);
            })
    }

}
